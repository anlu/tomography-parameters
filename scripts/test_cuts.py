#!/usr/bin/env python
"""
Script to optimize sync_part_x and voltage for .dat tomography files

Requires longitudinal_tomography==3.0.0 or higher.

Run as ./optimize_dat.py [path to .dat file]
"""

from os import path

import numpy as np
import numpy.fft as fft
import matplotlib.pyplot as plt
import pandas as pd
import tqdm
import yaml
import logging
from tomo.data import data_treatment as dtreat
from tomo.tomography import TomographyCpp
from tomo.utils import tomo_input as tin
from tomo.data import pre_process
from tomo.tracking import particles
# import tomo.cpp_routines.libtomo as libtomo
from tomo.utils import tomo_output as tout

import utils
from rf_programs.psb.psb_blondfactory import BLonDFactory
from tomography import track, make_machine, tomo_track

SIMULATE = False

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter(
    '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s',
    "%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)

log.addHandler(ch)

reconstr_idx = 0
rebin = 1
snpt = 2

# create noise:

dat = 'tomo_dat/BCMS25ns/R1ejref_2016.dat'
read_parameters, read_data = tin.get_user_input(dat)
machine, frames = tin.txt_input_to_machine(read_parameters)
waterfall = frames.to_waterfall(read_data)
profiles = tin.raw_data_to_profiles(waterfall, machine, frames.rebin,
                                    frames.sampling_time)
waterfall = profiles.waterfall
machine.values_at_turns()

n_bins = waterfall[0].size
n_profiles = waterfall.shape[0]

freq = np.linspace(0, 1 / frames.sampling_time, n_bins)
ft = fft.fft(waterfall)
filtered = utils.filter_frequencies(ft, threshold=0.12, invert=True)
ift = fft.ifft(filtered)

if SIMULATE:

    noise = np.tile(ift, (2, 4)).astype(np.float64)
    measured_waterfall = waterfall

    with open('rf_programs/rings.yml', 'r') as f:
        experiments = yaml.full_load(f)

    bcms_programs = experiments['PSB']['BCMS']

    with open(path.join('rf_programs', 'psb',
                        'beam_parameters.yml')) as f:
        default_beam_params = yaml.full_load(f)
    bcms_beam_params = default_beam_params['BCMS_extraction']

    # bcms_beam_params['n_slices'] = frames.nbins_frame
    factory = BLonDFactory(bcms_programs, **bcms_beam_params)

    profile, beam, ring, full_ring = factory.get_ring()

    (source_waterfall,
     turns_per_prof,
     dt, dE) = track(profile, ring, full_ring,
                     bcms_beam_params['tracking_duration'],
                     -1,
                     reconstr_idx,
                     n_profiles=100)

    bin_width = profile.bin_centers[1] - profile.bin_centers[0]

    machine = make_machine(ring, source_waterfall, factory, full_ring, -1,
                           bin_width, snpt, reconstr_idx, turns_per_prof)
    machine.values_at_turns()

else:
    source_waterfall = profiles.waterfall

    if machine.synch_part_x < 0:
        sync_bin, _, _ = pre_process.fit_synch_part_x(profiles)
        machine.synch_part_x = sync_bin

source_xp, source_yp = tomo_track(machine, reconstr_idx)

source_nbins = source_waterfall.shape[1]
max_left, max_right = utils.get_cuts(source_waterfall, margin=0)
results = []

try:
    for cut in tqdm.trange(int(min(max_left, source_nbins-max_right))):
        waterfall = source_waterfall.copy()
        waterfall = utils.cut_waterfall(waterfall, cut, source_nbins - cut)
        utils.show_mountainrange(source_waterfall, cut, source_nbins - cut)

        # add artificial noise and normalize
        if SIMULATE:
            waterfall *= measured_waterfall.sum() / waterfall.sum()
            noisy_waterfall = waterfall + noise[:waterfall.shape[0],
                                          :waterfall.shape[1]]

        machine.synch_part_x -= 1
        # machine.synch_part_y -= 1
        n_bins = waterfall.shape[1]
        machine.nbins = n_bins

        xp, yp = source_xp.copy(), source_yp.copy()
        xp, yp = utils.filter_particles(xp, yp, cut, source_nbins - cut)

        with utils.time_execution() as tomo_time:
            tomo = TomographyCpp(waterfall, xp, yp)
            tomo.run(30)

        if SIMULATE:
            with utils.time_execution() as t:
                tomo_noisy = TomographyCpp(noisy_waterfall, xp, yp)
                tomo_noisy.run(30)

            phase_space_blond = tout.create_phase_space_image(xp, yp,
                                                              tomo.weight,
                                                              n_bins,
                                                              reconstr_idx)

            phase_space_noisy = dtreat._make_phase_space(tomo_noisy.xp[:, reconstr_idx],
                                               tomo_noisy.yp[:, reconstr_idx],
                                               tomo_noisy.weight, n_bins)

            t_cent = machine.synch_part_x
            E_cent = machine.synch_part_y

            t_range = (np.arange(n_bins) - t_cent) * machine.dtbin
            E_range = (np.arange(n_bins) - E_cent) * machine.dEbin

            phase_space_noisy.clip(0.0)
            phase_space_noisy /= phase_space_noisy.sum()

            simulated_bins = utils.bin_simulated(t_range, E_range, dt, dE,
                                                 full_ring.RingAndRFSection_list[
                                                     0].rf_params,
                                                 ring, rec_turn=0)
            simulated_bins.clip(0.0)
            simulated_bins /= simulated_bins.sum()

            diff_blond_simulated = np.square(phase_space_blond - simulated_bins).sum()
            diff_noisy_simulated = np.square(phase_space_noisy - simulated_bins).sum()
            diff_noisy_blond = np.square(phase_space_noisy - phase_space_blond).sum()

            results.append({
                'blond-noisy': diff_noisy_blond,
                'blond-simulated': diff_blond_simulated,
                'noisy-simulated': diff_noisy_simulated,
                'time': t.duration,
                'cut': cut,
            })

            print(f'Cut: {cut}, noisy vs not noisy: {diff_noisy_blond}, noisy vs simulated: {diff_noisy_simulated} in {t.duration}s ')

            # tout.show(phase_space_blond, tomo_blond.diff, waterfall[reconstr_idx])
            # tout.show(phase_space_noisy, tomo_noisy.diff, noisy_waterfall[reconstr_idx])

            delta = phase_space_blond - phase_space_noisy
            deltaMin = np.min(delta)
            deltaMax = np.max(delta)
            deltaLim = np.max([np.abs(deltaMin), np.abs(deltaMax)])

            trueMax = np.max([np.max(phase_space_noisy), np.max(phase_space_blond), np.max(simulated_bins)])
            trueMin = np.min([np.min(phase_space_noisy[phase_space_noisy != 0]),
                              np.min(phase_space_blond[phase_space_blond != 0]),
                              np.min(simulated_bins[simulated_bins != 0])])

            f, (ax1, ax2, ax3) = plt.subplots(1, 3)
            f.set_size_inches(10, 5)
            ax1.contourf(t_range * 1E9, E_range / 1E6, phase_space_blond.T, cmap='copper',
                         levels=np.linspace(trueMin, trueMax, 100))
            ax1.set_title('BLonD waterfall phase space')
            ax2.contourf(t_range * 1E9, E_range / 1E6, phase_space_noisy.T, cmap='copper',
                         levels=np.linspace(trueMin, trueMax, 100))
            ax2.set_title('Noisy waterfall phase space')
            ax3.contourf(t_range * 1E9, E_range / 1E6, simulated_bins.T, cmap='copper',
                         levels=np.linspace(trueMin, trueMax, 100))
            ax3.set_title('Simulated bins phase space')
            plt.show()
        else:
            results.append({'time': tomo_time.duration,
                            'discr': tomo.diff[-1]})
except Exception:
    import traceback
    traceback.print_exc()
finally:
    print()
    results = pd.DataFrame(results)
    results.to_pickle('test_cuts.pkl')
    # np.save('source_waterfall.npy', source_waterfall)
    # np.save('noise.npy', noise)
