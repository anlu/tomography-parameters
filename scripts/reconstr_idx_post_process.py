"""
A script to investigate the effects on emittance values when changing
the reconstruction profile during tracking.
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import tqdm

import tomo.utils.tomo_input as tomoin
import tomo.utils.tomo_output as tomoout
import tomo.data.pre_process as pre_process
import tomo.data.data_treatment as dtreat
import tomo.data.post_process as post_process
import tomo.tracking.tracking as tracking
import tomo.tomography.tomography as tomography
import tomo.tracking.particles as parts
import tomo.shortcuts as shortcuts

import utils

dat = 'dat/R1ejref_2017.dat'

raw_params, raw_data = tomoin.get_user_input(dat)
machine, frames = tomoin.txt_input_to_machine(raw_params)

# set some tomography parameters manually
snpt = 2
rebin = 1
reference_rec_prof = 5

machine.snpt = snpt
frames.rebin = rebin

machine.values_at_turns()
waterfall = frames.to_waterfall(raw_data)  # waterfall cutting

profiles = tomoin.raw_data_to_profiles(
    waterfall, machine, frames.rebin, frames.sampling_time)

# some .dat files have an unset synchronous bin
if machine.synch_part_x == -1:
    sync_bin, _, _ = pre_process.fit_synch_part_x(profiles)
    machine.synch_part_x = sync_bin


xp, yp = shortcuts.track(machine, reference_rec_prof)
tomo = shortcuts.tomogram(profiles.waterfall, xp, yp, n_iter=20)

t_bins, E_bins, phase_space = dtreat.phase_space(tomo, machine,
                                                 reference_rec_prof)

ref_phase_space = phase_space.clip(0.)
ref_phase_space /= ref_phase_space.sum()
ref_emittance_90 = post_process.emittance_90(phase_space, t_bins, E_bins)
ref_emittance_rms = post_process.emittance_rms(phase_space, t_bins, E_bins)

results = []

for rec_prof in tqdm.tqdm(range(0, machine.nprofiles)):
    # xp, yp = shortcuts.track(machine, rec_prof)
    # tomo = shortcuts.tomogram(profiles.waterfall, xp, yp, n_iter=20)

    t_bins, E_bins, phase_space = dtreat.phase_space(tomo, machine, rec_prof)

    emittance_90 = post_process.emittance_90(phase_space, t_bins, E_bins)
    emittance_rms = post_process.emittance_rms(phase_space, t_bins, E_bins)

    diff_emittance_90 = ref_emittance_90 / emittance_90
    diff_emittance_rms = ref_emittance_rms / emittance_rms

    phase_space = phase_space.clip(0.)
    phase_space /= phase_space.sum()

    diff_phase_space = np.sum(np.abs(phase_space - ref_phase_space))

    results.append({
        'diff_emittance_rms': diff_emittance_rms,
        'diff_emittance_90': diff_emittance_90,
        'diff_phase_space': diff_phase_space,
        'rec_prof': rec_prof,
    })

results = pd.DataFrame(results)

fig, (ax1, ax2) = plt.subplots(1, 2)
ax1.plot(results['rec_prof'], results['diff_emittance_rms'], label='d e rms')
ax1.plot(results['rec_prof'], results['diff_emittance_90'], label='d e 90')
ax1.set_xlabel('Reconstruction profile')
ax1.set_ylabel('Difference between reference emittance and XXX')
ax1.legend()
ax2.plot(results['rec_prof'], results['diff_phase_space'])
ax2.set_xlabel('Reconstruction profile')
ax2.set_xlabel('Diff between reference and varied phase space')

plt.show()


