import logging
import os
import random
import time

import numpy as np
import tomo.data.data_treatment as dtreat
import yaml
from numpy_ringbuffer import RingBuffer
from tomo.data import post_process
from tomo.tomography import TomographyCpp

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter(
    '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s',
    "%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)

log.addHandler(ch)

# %% Load paths and settings to all data
this_dir = os.path.realpath(os.path.dirname(__file__))
resource_dir = os.path.join(os.path.split(this_dir)[0], 'ucap_data')

data = dict()
for obj in os.listdir(resource_dir):
    if os.path.isdir(obj):
        continue
    elif obj.endswith('.yml'):
        cycle, timeframe, _, idx = os.path.splitext(obj)[0].split('_')
        if cycle not in data:
            data[cycle] = dict()
        if timeframe not in data[cycle]:
            data[cycle][timeframe] = dict()
        with open(os.path.join(resource_dir, obj)) as f:
            loaded_data = yaml.full_load(f)

        path_keys = ('waterfall', 'xp', 'yp')
        for path_key in path_keys:
            data_path = os.path.join(resource_dir, loaded_data[path_key])
            if not os.path.isfile(data_path):
                raise FileNotFoundError(
                    'Data file at {} does not exist. '.format(
                        data_path
                    ))
            loaded_data[path_key] = data_path

        data[cycle][timeframe][int(idx)] = loaded_data
        log.info(f'Loaded {obj}')

for cycle, cycle_d in data.items():
    for timeframe, timeframe_d in cycle_d.items():
        cycle_d[timeframe] = [timeframe_d.get(ele, 0) for ele in range(len(timeframe_d))]

# %% input publisher

# cycle = random.choice(list(data.keys()))
# timeframe = random.choice(list(data[cycle].keys()))
cycle = 'LHC25'
timeframe = 'injection'
# input_set_selected = random.randrange(len(data[cycle][timeframe]))
input_set_selected = 4
data_selected = data[cycle][timeframe][input_set_selected]

loaded_waterfall = np.load(data_selected['waterfall'])
n_profiles = len(loaded_waterfall)

profile_nb_selected = data_selected['rec_prof']

log.info('Input publisher called: cycle={}, timeframe={}, '
         'input_set={}, profile_nb={}'.format(
    cycle, timeframe, input_set_selected, profile_nb_selected))

selected_waterfall = loaded_waterfall

# %% Tomo converter

xp_selected = np.load(data_selected['xp'])
yp_selected = np.load(data_selected['yp'])
nbins = data_selected['nbins']
dtbin = data_selected['dtbin']
dEbin = data_selected['dEbin']
sync_part_x = data_selected['sync_part_x']
sync_part_y = data_selected['sync_part_y']
energy = data_selected['energy']
momentum = data_selected['momentum']
mass = data_selected['mass']

# tomography routine
start_time = time.perf_counter()

# Tomography logic
niterations = data_selected['n_iter']

tomo = TomographyCpp(selected_waterfall)
tomo.xp = xp_selected
tomo.yp = yp_selected
weight = tomo.run(niter=niterations)

end_time = time.perf_counter()
tomo_time = start_time - end_time

# postprocessing
start_time = time.perf_counter()

phase_space = dtreat._make_phase_space(xp_selected[:, profile_nb_selected],
                                       yp_selected[:, profile_nb_selected],
                                       weight, nbins)

t_cent = sync_part_x
E_cent = sync_part_y

t_range = (np.arange(nbins) - t_cent) * dtbin
E_range = (np.arange(nbins) - E_cent) * dEbin

processed_data = post_process.post_process(phase_space, t_range, E_range,
                                           energy, momentum, mass)

emittance_rms = processed_data['emittance_rms']
emittance_90 = processed_data['emittance_90']
rms_dpp = processed_data['rms_dp/p']

end_time = time.perf_counter()
pp_time = end_time - start_time

# %% output checker

ring_buffers = dict()
statistics = dict()
for cycle, cycle_t in data.items():
    ring_buffers[cycle] = dict()
    statistics[cycle] = dict()
    for timeframe, timeframe_d in cycle_t.items():
        ring_buffers[cycle][timeframe] = [RingBuffer(capacity=100,
                                                    dtype=np.double)
                                          ] * len(timeframe_d)
        statistics[cycle][timeframe] = [dict()] * len(timeframe_d)

ring_buffer_selected = ring_buffers[cycle][timeframe][input_set_selected]
statistics_selected = statistics[cycle][timeframe][input_set_selected]

error_msg = ''
error_template = 'Incorrect value, {}: {} != {}\n'
# check result
if emittance_rms != data_selected['emittance_rms']:
    error_msg += error_template.format('emittance_rms',
                                       emittance_rms,
                                       data_selected['emittance_rms'])
if emittance_90 != data_selected['emittance_90']:
    error_msg += error_template.format('emittance_90',
                                       emittance_90,
                                       data_selected['emittance_90'])
if rms_dpp != data_selected['rms_dp/p']:
    error_msg += error_template.format('rms_dpp',
                                       rms_dpp,
                                       data_selected['rms_dp/p'])

if error_msg != '':
    log.error('Result was not correct for cycle={}, timeframe={}, '
              'input_set={}, profile_nb={}\n{}'
              .format(cycle, timeframe, input_set_selected,
                      profile_nb_selected, error_msg))

ring_buffer_selected.append(tomo_time)
latest_conversion_times = np.array(pp_time)
statistics_selected['input_set'] = input_set_selected
statistics_selected['profile_nb'] = profile_nb_selected
statistics_selected['cycle'] = cycle
statistics_selected['timeframe'] = timeframe
# statistics_selected['value_count'] = len(latest_conversion_times)
# statistics_selected['mean'] = np.mean(latest_conversion_times)
# statistics_selected['std'] = np.std(latest_conversion_times)
# statistics_selected['min'] = np.amin(latest_conversion_times)
# statistics_selected['max'] = np.amax(latest_conversion_times)
