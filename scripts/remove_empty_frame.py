import numpy as np
import matplotlib.pyplot as plt
import tomo.utils.tomo_input as tomoin

import utils


def filter_profiles(waterfall: np.ndarray):
    waterfall = waterfall / waterfall.sum()

    bin_sum = waterfall.sum(axis=1)

    threshold = bin_sum.max() - bin_sum.min() - bin_sum.mean()
    good_frames = bin_sum > threshold

    good_waterfall = waterfall[good_frames, :]

    return good_waterfall


dat = 'dat/R3InjDualHarmonic.dat'

with open(dat, 'r') as f:
    raw_params, raw_data = tomoin._split_input(f.readlines())

machine, frames = tomoin.txt_input_to_machine(raw_params)

# set some tomography parameters manually
snpt = 2
rebin = 1
rec_prof = 0

machine.snpt = snpt
frames.rebin = rebin

machine.values_at_turns()
waterfall = frames.to_waterfall(raw_data)  # waterfall cutting

profiles = tomoin.raw_data_to_profiles(
    waterfall, machine, frames.rebin, frames.sampling_time)

plt.pcolor(profiles.waterfall)
plt.show()
good_waterfall = filter_profiles(profiles.waterfall)
plt.pcolor(filter_profiles(profiles.waterfall))
plt.show()

utils.show_mountainrange(profiles.waterfall[:5, :], step_size=0)
utils.show_mountainrange(good_waterfall)


