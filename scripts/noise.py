import matplotlib.pyplot as plt
import numpy as np
import numpy.fft as fft
import yaml
from os import path
from sympy import fourier_series

import utils

dat = 'tomo_dat/BCMS25ns/R1ejref_2016.dat'

from tomo.utils import tomo_input as tin
from tomo import shortcuts
from tomography import track
from tomo.tomography import TomographyCpp
from tomo.utils import tomo_output as tout

from rf_programs.psb.psb_blondfactory import BLonDFactory

read_parameters, read_data = tin.get_user_input(dat)

machine, frames = tin.txt_input_to_machine(read_parameters)

machine.snpt = 2
frames.rebin = 2

machine.values_at_turns()

waterfall = frames.to_waterfall(read_data)

profiles = tin.raw_data_to_profiles(waterfall, machine, frames.rebin,
                                    frames.sampling_time)
profiles.calc_profilecharge()

waterfall = profiles.waterfall

plt.pcolor(waterfall)
plt.show()

utils.show_mountainrange(waterfall, step_size=0.01)

n_bins = waterfall[0].size
n_profiles = waterfall.shape[0]

# plot waterfall
# plt.plot(waterfall)
# plt.show()

freq = np.linspace(0, 1 / frames.sampling_time, n_bins)
ft = fft.fft(waterfall)

if len(ft.shape) == 1:
    plt.bar(freq[:n_bins // 2], np.abs(ft)[:n_bins // 2] * 1 / n_bins,
            width=1e7)
    plt.xlabel('Amplitude')
    plt.xlabel('Frequency [Hz]')
    plt.show()

_, filtered = utils.filter_frequencies(freq, ft, threshold=0.12)

if len(ft.shape) == 1:
    plt.bar(freq[:n_bins // 2], np.abs(filtered)[:n_bins // 2] * 1 / n_bins,
            width=1e7)
    plt.xlabel('Amplitude')
    plt.xlabel('Frequency [Hz]')
    plt.show()

# back to time space

ift = fft.ifft(filtered)

# %% plotting
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))
utils.show_mountainrange(waterfall, step_size=0.02, ax=ax1)
ax1.set_title('Before filtering')
utils.show_mountainrange(ift, step_size=0.02, ax=ax2)
ax2.set_title('After filtering')
plt.show()

# %% Tracking

reconstr_idx = machine.filmstart

if machine.synch_part_x < 0:
    machine.fit_synch_part_x(waterfall)

xp, yp = shortcuts.track(machine, reconstr_idx)

tomo_orig = TomographyCpp(profiles.waterfall, xp, yp)
tomo_orig.run(10)

tomo_filtered = TomographyCpp(ift, xp, yp)
tomo_filtered.run(10)

phase_space_orig = tout.create_phase_space_image(xp, yp, tomo_orig.weight,
                                                 machine.nbins, reconstr_idx)

phase_space_filtered = tout.create_phase_space_image(xp, yp,
                                                     tomo_filtered.weight,
                                                     machine.nbins,
                                                     reconstr_idx)

mse = np.square(phase_space_filtered - phase_space_orig).sum()

tout.show(phase_space_orig, tomo_orig.diff, waterfall[reconstr_idx])
tout.show(phase_space_filtered, tomo_filtered.diff, ift[reconstr_idx])

s = phase_space_filtered.sum() + phase_space_orig.sum()

print(f'MSE: {mse}')
print(f's: {s}')

# _, fourier_coeffs = utils.filter_frequencies(freq, ft, threshold=0.12,
#                                              invert=True)
#
# def f(x, Nh):
#    f = np.array([2*coeff*np.exp(1j*2*np.pi*x * freq[i]) for i, coeff in enumerate(fourier_coeffs[0])])
#    return f.sum(axis=0)
#
# N_points = 1000
# t = np.linspace(machine.min_dt, machine.max_dt, N_points)
# plt.plot(t, f(t, None))
# plt.show()

with open('rf_programs/rings.yml', 'r') as f:
    experiments = yaml.full_load(f)

bcms_programs = experiments['PSB']['BCMS']

with open(path.join('rf_programs', 'psb',
                    'beam_parameters.yml')) as f:
    default_beam_params = yaml.full_load(f)
bcms_beam_params = default_beam_params['BCMS_extraction']

bcms_beam_params['n_slices'] = frames.nbins_frame
factory = BLonDFactory(bcms_programs, **bcms_beam_params)

profile, beam, ring, full_ring = factory.get_ring()

(source_waterfall,
 turns_per_prof,
 dt, dE) = track(profile, ring, full_ring,
                 bcms_beam_params['tracking_duration'],
                 -1,
                 reconstr_idx,
                 n_profiles=n_profiles)

normalized_waterfall = np.array(source_waterfall) * waterfall.sum() / source_waterfall.sum()

machine.dturns = turns_per_prof
machine.values_at_turns()
tomo_blond = TomographyCpp(normalized_waterfall[:80], xp, yp)
tomo_blond.run(30)

phase_space_blond = tout.create_phase_space_image(xp, yp, tomo_blond.weight,
                                                  machine.nbins)

tout.show(phase_space_blond, tomo_blond.diff, normalized_waterfall[reconstr_idx])

print()

