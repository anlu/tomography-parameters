#!/usr/bin/env python
"""
Script to optimize sync_part_x and voltage for .dat tomography files

Requires longitudinal_tomography==3.0.0 or higher.

Run as ./optimize_dat.py [path to .dat file]
"""

import logging
from argparse import ArgumentParser
from copy import deepcopy
import matplotlib.pyplot as plt
from os import path

import numpy as np
import yaml
from tomo import shortcuts
from tomo.data import data_treatment as dtreat, pre_process
from tomo.utils import tomo_input as tomoin

import loops
import optimize
import utils

log = logging.getLogger()

log.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter(
    '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s',
    "%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)

log.addHandler(ch)

parser = ArgumentParser()
parser.add_argument('dat', help='Path to dat file')
parser.add_argument('--parameters', '-p', help='Path to parameters file')

args = parser.parse_args()

# read tomography parameters
with open(args.dat, 'r') as f:
    raw_params, raw_data = tomoin._split_input(f.readlines())

machine, frames = tomoin.txt_input_to_machine(raw_params)

# set some tomography parameters manually
snpt = 2
rebin = 1
rec_prof = 5

machine.snpt = snpt
frames.rebin = rebin

machine.values_at_turns()
waterfall = frames.to_waterfall(raw_data)  # waterfall cutting

profiles = tomoin.raw_data_to_profiles(
    waterfall, machine, frames.rebin, frames.sampling_time)

waterfall = utils.filter_profiles(profiles.waterfall)
machine.nprofiles = waterfall.shape[0]
profiles.waterfall = waterfall

sync_bin_set = False
if machine.synch_part_x == -1:
    sync_bin_set = True
    sync_bin, _, _ = pre_process.fit_synch_part_x(profiles)
    machine.synch_part_x = sync_bin

orig_machine = deepcopy(machine)

# some .dat files have an unset synchronous bin
opt_sync_bin, opt_volt1, opt_volt2, opt_phi12 = optimize.optimize_volt_syncbin(
    machine, profiles, rec_prof, n_iter=20, opt_v2=False)

machine.synch_part_x = opt_sync_bin
machine.vrf1 = opt_volt1
machine.vrf2 = opt_volt2
machine.phi12 = opt_phi12
cut_left, cut_right = optimize.optimize_cuts(machine, profiles.waterfall,
                                             rec_prof,
                                             n_iter=20)
waterfall = utils.cut_waterfall(waterfall, cut_left, cut_right)

print(
    'Optimized parameters: voltage1={} V, voltage2={}, sync_bin_x={}, '
    'phi12={}, left cut={}, right cut={}'.format(
        opt_volt1, opt_volt2, opt_sync_bin, opt_phi12, cut_left, cut_right
    ))

if args.parameters is not None:  # do further grid search on tomo parameters
    with open(args.parameters, 'r') as f:
        params = yaml.full_load(f)

    params['parameters']['sync_part_x'] = float(opt_sync_bin)
    params['parameters']['voltage'] = float(opt_volt1)

    basename = path.basename(args.parameters)
    tmp_param = '.opt_' + basename
    tmp_path = path.join(path.split(args.parameters)[0], tmp_param)

    with open(tmp_path, 'w') as f:
        yaml.dump(params, f)

    args.parameters = tmp_path

    df = loops.reconstruct_only(args)

    df.to_pickle('optimize_dat.pkl')
else:  # just do the reconstruction and plot
    # get a tomogram using the original parameters
    xp, yp = shortcuts.track(orig_machine, rec_prof)
    tomo_orig = shortcuts.tomogram(profiles.waterfall, xp, yp,
                                   n_iter=20)

    # apply the optimized parameters
    machine_opt = deepcopy(machine)

    machine_opt.vrf1 = opt_volt1
    machine_opt.vrf2 = opt_volt2
    machine_opt.phi12 = opt_phi12
    machine_opt.synch_part_x = opt_sync_bin

    machine_opt.synch_part_x -= cut_left
    n_bins = waterfall.shape[1]
    machine_opt.nbins = n_bins
    machine_opt.max_dt -= machine.dtbin * (
                profiles.waterfall.shape[1] - n_bins)

    machine_opt.values_at_turns()

    # get tomogram for optimized parameters
    xp, yp = shortcuts.track(machine_opt, rec_prof)
    tomo_opt = shortcuts.tomogram(waterfall, xp, yp, n_iter=20)

    log.info(f'Last discrepancy for original parameters: {tomo_orig.diff[-1]}')
    log.info(f'Last discrepancy for optimized parameters: {tomo_opt.diff[-1]}')
    if sync_bin_set:
        log.info(f'sync_bin_x was fitted by the optimization program')

    # make phase space histograms and plot
    t_bins, e_bins, phase_space_orig = dtreat.phase_space(tomo_orig, machine,
                                                          rec_prof)
    t_bins_opt, e_bins_opt, phase_space_opt = dtreat.phase_space(tomo_opt, machine_opt, rec_prof)

    norm_orig = phase_space_orig / np.sum(phase_space_orig)
    norm_opt = phase_space_opt / np.sum(phase_space_opt)

    trueMax = np.max([np.max(norm_opt), np.max(norm_orig)])
    trueMin = np.min([np.min(norm_opt[norm_opt != 0]),
                      np.min(norm_orig[norm_orig != 0])])

    f, (ax1, ax2) = plt.subplots(1, 2)
    f.set_size_inches(15, 5)
    ax1.contourf(t_bins_opt * 1E9, e_bins_opt / 1E6, norm_opt.T, cmap='hot_r',
                 levels=np.linspace(trueMin, trueMax, 100))
    ax1.set_title('Optimized parameters')
    ax2.contourf(t_bins * 1E9, e_bins / 1E6, norm_orig.T,
                 cmap='hot_r', levels=np.linspace(trueMin, trueMax, 100))
    ax2.set_title('Original parameters')

    plt.tight_layout()
    plt.show()
