#!/usr/bin/env python
"""
Loads a .dat file, shows the mountainrange plot, and then the computed
tomogram

Requires longitudinal_tomography==3.3.0 or higher.

Run as ./show_dat.py [path to .dat file]
"""

from argparse import ArgumentParser
from copy import deepcopy

import matplotlib.pyplot as plt
import numpy as np
import yaml
from scipy import optimize as opt
from tomo.data import data_treatment as dtreat, pre_process
from tomo.tomography import tomography
from tomo.tracking import Machine
from tomo.tracking import particles as parts
from tomo.tracking import tracking
from tomo.utils import tomo_input as tomoin
from tomo.utils import tomo_output as tomoout
from os import path
import loops
import utils

parser = ArgumentParser()
parser.add_argument('dat', help='Path to dat file')

args = parser.parse_args()

# read tomography parameters
with open(args.dat, 'r') as f:
    raw_params, raw_data = tomoin._split_input(f.readlines())

machine, frames = tomoin.txt_input_to_machine(raw_params)

# set some tomography parameters manually
snpt = 2
rebin = 1
rec_prof = 10

machine.snpt = snpt
frames.rebin = rebin

machine.values_at_turns()
waterfall = frames.to_waterfall(raw_data)  # waterfall cutting

profiles = tomoin.raw_data_to_profiles(
    waterfall, machine, frames.rebin, frames.sampling_time)

# some .dat files have an unset synchronous bin
if machine.synch_part_x == -1:
    sync_bin, _, _ = pre_process.fit_synch_part_x(profiles)
    machine.synch_part_x = sync_bin

tracker = tracking.Tracking(machine)
initxp, inityp = tracker.track(rec_prof)

xp, yp = parts.physical_to_coords(
    initxp, inityp, machine,
    tracker.particles.xorigin,
    tracker.particles.dEbin)

xp, yp = parts.ready_for_tomography(xp, yp, machine.nbins)

tomo = tomography.TomographyCpp(profiles.waterfall, xp, yp)

tomo.run(niter=20)

f, ax = plt.subplots()
utils.show_mountainrange(profiles.waterfall, ax=ax)
plt.show()

plt.pcolor(profiles.waterfall)
image = tomoout.create_phase_space_image(xp, yp, tomo.weight, machine.nbins, rec_prof)

data = {
    'waterfall': profiles.waterfall,
    'weights': tomo.weight,
    'n_bins': machine.nbins,
    'rec_prof': rec_prof,
    'discr': tomo.diff,
    'image': image
}
with open('tomo_data', 'wb') as f:
    import pickle
    pickle.dump(data, f)

tomoout.show(image, tomo.diff, profiles.waterfall[rec_prof, :])
