#!/usr/bin/env python
"""
Script to optimize sync_part_x and voltage for .dat tomography files

Requires longitudinal_tomography==3.0.0 or higher.

Run as ./optimize_dat.py [path to .dat file]
"""

from os import path

import numpy as np
import numpy.fft as fft
import matplotlib.pyplot as plt
import pandas as pd
import tqdm
import yaml
import logging
from tomo.data import data_treatment as dtreat
from tomo.tomography import TomographyCpp
from tomo.utils import tomo_input as tin
from tomo.data import pre_process
from tomo.tracking import particles
# import tomo.cpp_routines.libtomo as libtomo
from tomo.utils import tomo_output as tout

import utils
from rf_programs.psb.psb_blondfactory import BLonDFactory
from tomography import track, make_machine, tomo_track

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter(
    '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s',
    "%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)

log.addHandler(ch)

reconstr_idx = 0
rebin = 1
snpt = 2

# create noise:

dat = 'tomo_dat/BCMS25ns/R1ejref_2016.dat'
read_parameters, read_data = tin.get_user_input(dat)
machine, frames = tin.txt_input_to_machine(read_parameters)
# frames.skip_bins_start, frames.skip_bins_end = 0, 0
frames.rebin = 1
# machine.max_dt += (frames.nbins_frame - machine.nbins) * machine.dtbin

waterfall = frames.to_waterfall(read_data)
profiles = tin.raw_data_to_profiles(waterfall, machine, frames.rebin,
                                    frames.sampling_time)
waterfall = profiles.waterfall
machine.values_at_turns()

n_bins = waterfall[0].size
n_profiles = waterfall.shape[0]

freq = np.linspace(0, 1 / frames.sampling_time, n_bins)

ft = fft.fft(waterfall)
filtered = utils.filter_frequencies(ft, threshold=0.12)
ift = fft.ifft(filtered)

source_waterfall = profiles.waterfall
if machine.synch_part_x < 0:
    sync_bin, _, _ = pre_process.fit_synch_part_x(profiles)
    machine.synch_part_x = sync_bin

source_xp, source_yp = tomo_track(machine, reconstr_idx)

source_nbins = source_waterfall.shape[1]
results = []

try:
    for margin in range(40, -1, -1):
        waterfall = source_waterfall.copy()
        xp, yp = source_xp.copy(), source_yp.copy()

        with utils.time_execution() as filter_time:
            cut_left, cut_right = utils.get_cuts(ift, margin=margin)

            xp, yp = utils.filter_particles(xp, yp, cut_left, cut_right)

            waterfall = utils.cut_waterfall(waterfall, cut_left, cut_right)
        utils.show_mountainrange(source_waterfall, cut_left, cut_right)

        machine.synch_part_x -= cut_left
        n_bins = waterfall.shape[1]
        machine.nbins = n_bins

        with utils.time_execution() as tomo_time:
            tomo = TomographyCpp(waterfall, xp, yp)
            tomo.run(30)

        results.append({'time': tomo_time.duration,
                        'margin': margin,
                        'filter_time': filter_time.duration,
                        'discr': tomo.diff[-1]})
except Exception:
    import traceback
    traceback.print_exc()
finally:
    print()
    results = pd.DataFrame(results)
    results.to_pickle('test_cuts.pkl')
    # np.save('source_waterfall.npy', source_waterfall)
    # np.save('noise.npy', noise)
