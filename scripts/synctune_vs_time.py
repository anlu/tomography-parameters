import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as cont


import blond.beam.beam as beam
import blond.beam.distributions as distBeam
from blond.beam.beam import Proton, Beam
import blond.trackers.tracker as tracker
from blond.input_parameters import ring_options as ringOpt, ring as inputRing, \
    rf_parameters as inputRF

momentum = np.load('../psb-rf-programs/MomentumPrograms/1_4GeVMomentum.npy')
voltage = np.load('../psb-rf-programs/BCMS/fullCycleVoltageFunction1_5eVs.npy')
phase = np.load('../psb-rf-programs/BCMS/fullCyclePhaseFunction1_5eVs.npy')

radius = 25
momentum_compaction = 0.06016884
gamma_transition = 1 / np.sqrt(momentum_compaction)
C = 2 * np.pi * radius  # [m]
particle_type = 'proton'
bending_radius = 8.239

n_macro = 1e6
emittance = 1.4
mu = 1

BField = 0.86128
# momentum = 1 * BField * bending_radius * cont.c

time = momentum[0, :]
momentum = momentum[1, :]

ring_options = ringOpt.RingOptions("linear", t_start=time[0],
                                   t_end=time[-1])
ring = inputRing.Ring(C, momentum_compaction,
                      (time, momentum),
                      Proton(), RingOptions=ring_options)


my_beam = beam.Beam(ring, n_macro, 0)

time = voltage[0, :]

voltage_function = ([time, voltage[1, :]], [time, voltage[2, :]])
phase_function = ([time, phase[1, :]], [time, phase[2, :]])


rf_params = inputRF.RFStation(ring, [1, 2], voltage_function,
                              phase_function, 2)

# %% tracker

long_tracker = tracker.RingAndRFTracker(rf_params, my_beam)
full_ring = tracker.FullRingAndRF([long_tracker])

# %% match beam

beamDist = distBeam.matched_from_distribution_function(my_beam, full_ring,
                                                       distribution_exponent=mu,
                                                       distribution_type='binomial',
                                                       emittance=emittance)

time_voltage = voltage[0, :]
voltage1 = voltage[1, :]

eta = ring.eta_0[0][0]
beta = ring.beta[0][0]
energy = ring.energy[0][0]
syncTune = np.sqrt(
    (1 * 1 * voltage1 * np.abs(eta * 1)) / (2 * np.pi * beta ** 2 * energy))
syncPeriod = ring.t_rev[0] / syncTune
sync_freq = 1/syncPeriod

plt.plot(time_voltage * 1e3, syncPeriod * 1e3)
plt.title('Synchrotron period vs time')
plt.xlabel('Time (ms)')
plt.ylabel('Synchrotron period (ms)')

plt.show()

plt.plot(time_voltage * 1e3, sync_freq * 1e-3)
plt.title('Synchrotron frequency vs time')
plt.xlabel('Time (ms)')
plt.ylabel('Synchrotron frequency (kHz)')

plt.show()
