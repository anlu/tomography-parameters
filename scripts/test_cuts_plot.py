import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

results = pd.read_pickle('test_cuts.pkl')

# if 'discr' in results and 'margin' not in results:
#     fig, (ax1, ax2) = plt.subplots(1, 2)
#     fig.set_size_inches(10, 5)

#     n_bins = 240
#     bins_cut = np.arange(len(results['time']))
#     bins_cut = n_bins - 2*bins_cut
#     ax1.plot(bins_cut, results['time'])
#     ax1.set_xlabel('#bins')
#     ax1.set_ylabel('reconstruction time (s)')
#     ax2.plot(bins_cut, results['discr'])
#     ax2.set_xlabel('#bins')
#     ax2.set_ylabel('final discrepancy')
#     plt.show()
# elif 'margin' in results:
fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
fig.set_size_inches(15, 5)

ax1.plot(results['margin'], results['time'])
ax1.set_xlabel('margin')
ax1.set_ylabel('reconstruction time (s)')
ax2.plot(results['margin'], results['filter_time'])
ax2.set_xlabel('margin')
ax2.set_ylabel('Time to find and set cuts, filter particles (s)')
ax3.plot(results['margin'], results['discr'])
ax3.set_xlabel('margin')
ax3.set_ylabel('final discrepancy')
plt.show()
# else:
#     fig, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4)
#     fig.set_size_inches(20, 5)

#     n_bins = 799
#     bins_cut = np.arange(len(results['time']))
#     bins_cut = n_bins - 2*bins_cut
#     ax1.plot(bins_cut, results['time'])
#     ax1.set_title('#bins vs tomography time')
#     ax2.plot(bins_cut, results['blond-noisy'])
#     ax2.set_title('#bins vs noisy-not noisy waterfall')
#     ax3.plot(bins_cut, results['blond-simulated'])
#     ax3.set_title('#bins vs not noisy waterfall - binned phase space')
#     ax4.plot(bins_cut, results['noisy-simulated'])
#     ax4.set_title('#bins vs noisy waterfall - binned phase space')
#     plt.show()