"""
Script to develop plotting of tomogram and profiles in a similar way to the
tomoscope in CCM.
"""

import pickle
import os.path as path
import tomo.utils.tomo_input as tin
import tomo.utils.tomo_output as tout
import tomo.utils.tomo_run as run
import tomo.shortcuts as shortcuts
import tomo.data.data_treatment as dtreat
from matplotlib.figure import Figure
from matplotlib.axes import Axes
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np

TOMO_DATA = 'tomo_data.pkl'
REC_PROF = 10

if not path.isfile(TOMO_DATA):
    raw_params, raw_data = tin.get_user_input('dat/R3InjDualHarmonic.dat')
    machine, frames = tin.txt_input_to_machine(raw_params)
    machine.values_at_turns()
    waterfall = frames.to_waterfall(raw_data)
    profiles = tin.raw_data_to_profiles(waterfall, machine, frames.rebin,
                                        frames.sampling_time)
    if profiles.machine.synch_part_x < 0:
        fit_info = dtreat.fit_synch_part_x(profiles)
        machine.load_fitted_synch_part_x_ftn(fit_info)

    waterfall = profiles.waterfall
    xp, yp = shortcuts.track(machine, REC_PROF)
    tomo = shortcuts.tomogram(waterfall, xp, yp, 60)
    discr = tomo.diff

    t_range, E_range, phase_space = dtreat.phase_space(tomo, machine, REC_PROF)

    # Removing (if any) negative areas.
    phase_space = phase_space.clip(0.0)
    # Normalizing phase space.
    phase_space /= np.sum(phase_space)

    output = {
        'waterfall': waterfall,
        'n_bins': machine.nbins,
        'rec_prof': REC_PROF,
        'discr': tomo.diff,
        'weights': tomo.weight,
        'phase_space': phase_space,
        't_range': t_range,
        'E_range': E_range,
    }

    with open(TOMO_DATA, 'wb') as f:
        pickle.dump(output, f)
else:
    with open(TOMO_DATA, 'rb') as f:
        data = pickle.load(f)

    waterfall = data['waterfall']
    n_bins = data['n_bins']
    rec_prof = data['rec_prof']
    discr = data['discr']
    weights = data['weights']
    phase_space = data['phase_space']
    t_range = data['t_range']
    E_range = data['E_range']


def plot(phase_space: np.ndarray, discrepancy: np.ndarray,
         measured_profile: np.ndarray, figure: Figure = None,
         big_screen: bool = False):
    # Creating plot
    gs = gridspec.GridSpec(4, 4)

    if figure is None:
        do_show = True
        figure = plt.figure()
    else:
        do_show = False
        figure.clf()

    img: Axes = figure.add_subplot(gs[1:, :-1])
    profs1: Axes = figure.add_subplot(gs[0, :-1])
    profs2: Axes = figure.add_subplot(gs[1:, -1])
    convg: Axes = figure.add_subplot(gs[0, -1])
    # cax: Axes = figure.add_subplot(gs[1:, 0])

    cimg = img.contourf(t_range*1e9, E_range/1e6, phase_space.T, cmap='hot_r',
                        levels=100)
    img.yaxis.set_ticks_position('left')
    img.set_xlabel('[ns]')
    img.set_ylabel('[MeV]')
    img.xaxis.set_major_locator(plt.MaxNLocator(3))
    img.yaxis.set_major_locator(plt.MaxNLocator(5))
    # cbar = figure.colorbar(cimg, ax=img, cax=cax, fraction=0.046, pad=0.04)
    # cbar.set_ticks(np.linspace(phase_space.min(), phase_space.max(), 5))
    # cax.yaxis.set_ticks_position('left')

    for ax in (profs1, profs2, convg):
        ax.set_xticks([])
        ax.set_yticks([])

    time_proj = np.sum(phase_space, axis=1)
    profs1.plot(time_proj, label='reconstructed', zorder=5, color='black')
    profs1.plot(measured_profile, label='measured', zorder=0, color='tab:red')
    profs1.set_ylabel('[A]')

    abs_max = max(time_proj.max(), measured_profile.max())
    abs_min = min(time_proj.min(), measured_profile.min())
    profs1.set_yticks(np.linspace(abs_min, abs_max, 3))

    energy_proj = np.sum(phase_space, axis=0)
    profs2.plot(energy_proj, np.arange(energy_proj.shape[0]), color='black')
    profs2.set_xlabel('[e/eV]')
    profs2.set_xticks(np.linspace(energy_proj.min(), energy_proj.max(), 3))

    convg.plot(discrepancy, label='discrepancy', color='black')
    convg.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

    convg.set_xticks(np.arange(len(discrepancy)))
    convg.set_xticklabels([])

    if do_show:
        plt.show()


measured_profile = waterfall[REC_PROF, :]
measured_profile[:] /= np.sum(measured_profile)

plot(phase_space, discr, measured_profile)
