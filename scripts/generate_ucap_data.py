import itertools
from os import path

import numpy as np
import yaml
import copy
from tomo.data import post_process
from tomo.data.data_treatment import phase_space as make_phase_space
from tomo.utils import tomo_input as tin

from rf_programs.psb.psb_blondfactory import BLonDFactory
from tomography import track, make_machine, tomo_track, run_tomo
from utils import mkdir_p

with open('rf_programs/rings.yml') as f:
    ring_parameters = yaml.full_load(f)
ring_parameters = ring_parameters['PSB']

with open('rf_programs/psb/beam_parameters.yml') as f:
    beam_parameters = yaml.full_load(f)

with open('scripts/tomography_settings.yml') as f:
    reconstruction_parameters = yaml.full_load(f)

default_reconstruction_params = {
    'rec_prof': 0,
    'snpt': 1,
    'rebin': 1,
    'n_iter': 30,
    'sync_part_x': -1,
    'turns_per_prof': -1
}

for key, val in default_reconstruction_params.items():
    for cycle, cycle_params in reconstruction_parameters.items():
        if key not in cycle_params:
            cycle_params[key] = val

param_factors = {
    'emittance': [0.9, 1.0, 1.1],
    'mu': [0.9, 1.0, 1.1]
}

keys = param_factors.keys()
values = param_factors.values()

HERE = path.split(path.abspath(__file__))[0]
PARENT_FOLDER = path.split(HERE)[0]
data_folder = path.join(PARENT_FOLDER, 'ucap_data')
mkdir_p('ucap_data')

for cycle, cycle_parameters in beam_parameters.items():
    if cycle == 'MTE_extraction':
        continue
    cycle_folder = path.join(data_folder, cycle)
    mkdir_p(cycle_folder)
    for i, vals in enumerate(itertools.product(*values)):
        beam_factors = dict(zip(keys, vals))

        instance = dict(cycle_parameters)
        instance['emittance'] *= beam_factors['emittance']
        instance['mu'] *= beam_factors['mu']
        instance = {**instance, **reconstruction_parameters[cycle]}

        factory = BLonDFactory(ring_parameters[cycle.split('_')[0]],
                               **cycle_parameters)

        profile, beam, ring, full_ring = factory.get_ring()

        if 'tracking_start' in instance:
            tracking_start = instance['tracking_start']
        else:
            tracking_start = None

        (source_waterfall,
         turns_per_prof,
         dt, dE) = track(profile, ring, full_ring,
                         tracking_start=tracking_start, **instance)

        waterfall = source_waterfall.copy()
        bin_width = profile.bin_centers[1] - profile.bin_centers[0]
        instance['bin_width'] = bin_width
        instance['turns_per_prof'] = turns_per_prof
        rec_turn = instance['rec_prof'] * turns_per_prof

        machine = make_machine(ring, waterfall, factory,
                               full_ring,
                               **instance)
        orig_machine = copy.deepcopy(machine)

        profiles = tin.raw_data_to_profiles(waterfall, machine,
                                            instance['rebin'], machine.dtbin,
                                            machine.synch_part_x)
        waterfall = profiles.waterfall

        instance['sync_part_x'] = machine.synch_part_x

        xp, yp = tomo_track(machine, instance['rec_prof'])

        tomo, weight = run_tomo(waterfall, xp, yp, machine,
                                instance['rec_prof'],
                                instance['n_iter'])

        (t_range,
         E_range,
         phase_space) = make_phase_space(tomo, machine,
                                         instance['rec_prof'])

        momentum = ring.momentum[0, rec_turn]
        energy = ring.energy[0, rec_turn]
        mass = ring.Particle.mass

        emittance_values = post_process.post_process(phase_space,
                                                     t_range, E_range,
                                                     energy, momentum, mass)

        # prepare for yaml dump
        paths = {
            'waterfall': path.join(cycle, f'{cycle}_waterfall_{i}.npy'),
            'xp': path.join(cycle, f'{cycle}_xp.npy'),
            'yp': path.join(cycle, f'{cycle}_yp.npy'),
        }

        # make an organised yaml dump
        to_dump_tracking = instance.copy()
        to_dump_tomo = dict()
        tomo_parameters = ('rebin', 'snpt', 'n_iter', 'rec_prof')
        for param in tomo_parameters:
            to_dump_tomo[param] = to_dump_tracking[param]
            del to_dump_tracking[param]

        ignore_tracking_parameters = ('cut_left', 'cut_right', 'n_slices', 'bin_width')
        for param in ignore_tracking_parameters:
            if param in to_dump_tracking:
                del to_dump_tracking[param]

        # convert numpy floats to python
        to_dump = {
            'tracking': {**to_dump_tracking,
                         'sync_part_y': orig_machine.synch_part_y,
                         'nbins': orig_machine.nbins,
                         'dtbin': orig_machine.dtbin,
                         'dEbin': orig_machine.dEbin,
                         },
            'tomography': to_dump_tomo,
            'physics': {
                'momentum': momentum,
                'energy': energy,
                'mass': mass,
            },
            'correct_output': emittance_values,
            'paths': paths,
        }

        # convert numpy scalars to python versions for yaml dump
        def to_float(d: dict):
            for k, v in d.items():
                if isinstance(v, dict):
                    to_float(v)
                elif isinstance(v, np.float_):
                    d[k] = float(v)
                elif isinstance(v, np.int_):
                    d[k] = int(v)


        to_float(to_dump)

        machine_args = {
            'dturns': orig_machine.dturns,
            'vrf1': orig_machine.vrf1,
            'vrf2': orig_machine.vrf2,
            'vrf1dot': orig_machine.vrf1dot,
            'vrf2dot': orig_machine.vrf2dot,
            'phi12': orig_machine.phi12,
            'h_ratio': orig_machine.h_ratio,
            'mean_orbit_rad': orig_machine.mean_orbit_rad,
            'bending_rad': orig_machine.bending_rad,
            'b0': orig_machine.b0,
            'bdot': orig_machine.bdot,
            'trans_gamma': orig_machine.trans_gamma,
            'rest_energy': orig_machine.e_rest,
            'nprofiles': orig_machine.nprofiles,
            'nbins': orig_machine.nbins,
            'synch_part_x': orig_machine.synch_part_x,
            'dtbin': orig_machine.dtbin,
            'dEbin': machine.dEbin,
            'snpt': orig_machine.snpt,
        }
        to_float(machine_args)

        # save all parameters and postprocessing values in a YAML file
        parameter_file_name = path.join(cycle_folder,
                                        f'{cycle}_extra_parameters_{i}.yml')
        with open(parameter_file_name, 'w') as f:
            yaml.dump(to_dump, f, sort_keys=False)

        np.save(path.join(data_folder, paths['waterfall']), source_waterfall)
        if i == 4:  # only save xp and yp for the ideal emittance / mu
            np.save(path.join(data_folder, paths['xp']), xp)
            np.save(path.join(data_folder, paths['yp']), yp)

            machine_args_file_name = path.join(cycle_folder,
                                               f'{cycle}_machine_args.yml')
            with open(machine_args_file_name, 'w') as f:
                yaml.dump(machine_args, f, sort_keys=False)

            tomo_parameters_file = path.join(cycle_folder,
                                             f'{cycle}_parameters.yml')
            with open(tomo_parameters_file, 'w') as f:
                yaml.dump(to_dump_tomo, f,
                          sort_keys=False)
