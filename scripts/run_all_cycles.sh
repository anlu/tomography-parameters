#!/bin/bash

set -e

python main.py simulation -a psb -c BCMS -t injection
python main.py simulation -a psb -c BCMS -t extraction
python main.py simulation -a psb -c ISOLDE -t injection
python main.py simulation -a psb -c ISOLDE -t extraction
python main.py simulation -a psb -c LHC25 -t injection
python main.py simulation -a psb -c LHC25 -t extraction
python main.py simulation -a psb -c MTE -t injection
#python main.py simulation -a PSB -c MTE -t extraction
python main.py simulation -a psb -c TOF -t injection
python main.py simulation -a psb -c TOF -t extraction
