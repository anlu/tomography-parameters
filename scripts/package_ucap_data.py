import logging
import os
from pathlib import Path
import re
import shutil
from argparse import ArgumentParser
from os import path

log = logging.getLogger(__name__)

SIM_DATA_NAMES = ('waterfall', 'extra_parameters')
TRK_DATA_NAMES = ('parameters', 'machine_args')
IGNORE_NAMES = ('xp', 'yp')
output_dir = 'output'


def mkdir_p(dir: str):
    Path(dir).mkdir(parents=True, exist_ok=True)


def split_data(data_root: str):
    data_root = args.data
    if not path.isdir(data_root):
        raise ValueError(f'{data_root} does not exist or is not a directory')

    simulated_data_dir = path.join(output_dir, 'simulated', 'resources')
    mkdir_p(output_dir)
    mkdir_p(simulated_data_dir)

    file_regex = r'(?P<name>[a-zA-z]+)(_(?P<idx>\d+))?\.(?P<ext>[a-zA-Z]{2,3})'

    for cycle_dir in os.listdir(data_root):
        cycle, timeframe = cycle_dir.split('_')

        tracked_data_dir = path.join(output_dir,
                                     f'tracked_{cycle}',
                                     'resources')
        mkdir_p(tracked_data_dir)
        mkdir_p(path.join(tracked_data_dir, cycle_dir))
        mkdir_p(path.join(simulated_data_dir, cycle_dir))

        regex = re.compile(rf'{cycle_dir}_' + file_regex)
        for file in os.listdir(path.join(data_root, cycle_dir)):
            match = regex.match(file)
            if not match:
                raise ValueError('No matches for regex.')

            name = match.group('name')
            ext = match.group('ext')
            idx = match.group('idx')

            if name in SIM_DATA_NAMES:
                shutil.copy(path.join(data_root, cycle_dir, file),
                            path.join(simulated_data_dir, cycle_dir, file))
            elif name in TRK_DATA_NAMES:
                shutil.copy(path.join(data_root, cycle_dir, file),
                            path.join(tracked_data_dir, cycle_dir, file))
            else:
                log.info('No matches: leaving file {} as-is.'.format(
                    path.join(data_root, cycle_dir, file)
                ))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('data', type=str,
                        help='Path to root folder of generated UCAP data')
    args = parser.parse_args()

    split_data(args.data)
