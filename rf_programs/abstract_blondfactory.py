from numbers import Number
from typing import Tuple, List
import numpy as np
import logging
import re

from blond.beam.beam import Beam
from blond.beam.profile import Profile
from blond.input_parameters.ring import Ring
from blond.trackers.tracker import FullRingAndRF

log = logging.getLogger(__name__)


class AbstractBLonDFactory:
    """
    This class serves as a template for a what a BLonD Factory should look like

    The input parameters are required for simulating a 2-harmonic synchrotron
    and your custom class should inherit from this class for best
    compatibility. The only requirement for the custom class is to
    initialise this super class and the `get_ring` method.
    """
    def __init__(self,
                 momentum_compaction: Number,
                 radius: float,
                 bending_radius: Number,
                 h_ratio: Number,
                 harmonics: List[int],
                 emittance: float,
                 mu: float,
                 turns_per_prof: int = None,
                 n_slices: int = None,
                 n_profiles: int = None,
                 n_macro: Number = None,
                 initial_time: Number = None,
                 final_time: Number = None,
                 cut_left: Number = None,
                 cut_right: Number = None,
                 **kwargs):

        self.momentum_compaction = momentum_compaction
        self.gamma_transition = 1 / np.sqrt(momentum_compaction)

        self.turns_per_prof = turns_per_prof
        self.n_profiles = n_profiles
        self.n_slices = n_slices
        self.initial_time = initial_time
        self.final_time = final_time

        self.cut_left = cut_left
        self.cut_right = cut_right

        self.radius = radius
        self.circumference = 2 * radius * np.pi
        self.bending_radius = bending_radius

        self.n_macro = n_macro
        self.particle_type = 'proton'
        self.emittance = emittance
        self.mu = mu

        self.h_ratio = h_ratio
        self.harmonics = harmonics

    def get_ring(self) -> Tuple[Profile, Beam, Ring, FullRingAndRF]:
        pass
