import logging
from os import path
from typing import Dict, Tuple

import numpy as np
from blond.beam import distributions as distBeam
from blond.beam.beam import Beam, Proton
from blond.beam.profile import CutOptions, Profile
from blond.input_parameters.rf_parameters import RFStation
from blond.input_parameters.ring import Ring
from blond.input_parameters.ring_options import RingOptions
from blond.trackers.tracker import FullRingAndRF, RingAndRFTracker
import re
from yaml import full_load

from ..abstract_blondfactory import AbstractBLonDFactory

log = logging.getLogger(__file__)


class BLonDFactory(AbstractBLonDFactory):
    def __init__(self, string_params: Dict[str, str], **kwargs):

        HERE = path.split(path.abspath(__file__))[0]

        # get common hardware parameters
        with open(path.join(HERE, 'common_parameters.yml'), 'r') as f:
            common_parameters = full_load(f)

        # Overwrite loaded parameters with passed arguments
        all_args = kwargs

        for param, val in common_parameters.items():
            if param not in all_args:
                all_args[param] = val

        # load and construct voltage, phase and momentum programs
        voltage = np.load(string_params['voltage'])
        phase = np.load(string_params['phase'])
        momentum = np.load(string_params['momentum'])

        voltage_time = voltage[0, :]
        self.voltage_function = ([voltage_time, voltage[1, :]],
                                 [voltage_time, voltage[2, :]])

        phase_time = phase[0, :]
        self.phase_function = ([phase_time, phase[1, :]],
                               [phase_time, phase[2, :]])

        self.momentum_function = (momentum[0, :], momentum[1, :])

        # initialise class with all params including overrides
        super().__init__(**all_args)

    def get_ring(self) -> Tuple[Profile, Beam, Ring, FullRingAndRF]:

        ring_options = RingOptions("linear")

        if self.initial_time is not None:
            ring_options.t_start = self.initial_time
        if self.final_time is not None:
            ring_options.t_end = self.final_time

        ring = Ring(self.circumference, self.momentum_compaction,
                    self.momentum_function,
                    Proton(), RingOptions=ring_options)

        n_turns = ring.n_turns
        log.debug(f'Created ring with {n_turns} turns')

        beam = Beam(ring, self.n_macro, 0)

        rf_params = RFStation(ring, self.harmonics,
                              self.voltage_function,
                              self.phase_function, len(self.harmonics))

        long_tracker = RingAndRFTracker(rf_params, beam)
        full_ring = FullRingAndRF([long_tracker])

        beamDist = distBeam.matched_from_distribution_function(beam,
                                                               full_ring,
                                                               distribution_exponent=self.mu,
                                                               distribution_type='binomial',
                                                               emittance=self.emittance)

        # set dynamic n_slices to make it somewhat constant wrt B field
        if self.n_slices is None or self.n_slices == -1:
            bunch_length = beam.dt.max() - beam.dt.min()
            bin_width = bunch_length / 200
            n_slices = int(ring.t_rev[0] / bin_width)
            log.debug(f'Set n_slices to {n_slices}')
        else:
            n_slices = self.n_slices

        cutOpts = CutOptions(0, ring.t_rev[0], n_slices)
        cutOpts.cut_right = ring.t_rev[0]

        cut_regex = re.compile(r'(?P<factor>[\d\.]+)\s*\*\s*t_rev')
        if self.cut_left is not None:
            if 't_rev' in self.cut_left:
                match = cut_regex.match(self.cut_left)
                factor = float(match.group('factor'))
                cutOpts.cut_left = factor * ring.t_rev[0]
            else:
                cutOpts.cut_left = self.cut_left
        if self.cut_right is not None:
            if 't_rev' in self.cut_right:
                match = cut_regex.match(self.cut_right)
                factor = float(match.group('factor'))
                cutOpts.cut_right = factor * ring.t_rev[0]
            else:
                cutOpts.cut_right = self.cut_right

        cutOpts.set_cuts()

        profile = Profile(beam, cutOpts)

        profile.set_slices_parameters()
        profile.track()

        return profile, beam, ring, full_ring
