from numbers import Number

import numpy as np
from blond.beam.beam import Beam
from blond.beam.profile import Profile
from blond.input_parameters.rf_parameters import RFStation
from blond.input_parameters.ring import Ring
from blond.trackers.tracker import FullRingAndRF
from tomo.tomography import TomographyCpp
from rf_programs.abstract_blondfactory import AbstractBLonDFactory

from tomo.tracking import Machine, Tracking
import tomo.tracking.particles as parts
import tomo.data.profiles as dprofs
import tomo.data.pre_process as pre_process
import tomo.data.data_treatment as treat
import scipy.constants as cont

import logging

from utils import time_execution

log = logging.getLogger(__name__)


def track(profile: Profile, ring: Ring,
          full_ring: FullRingAndRF,
          tracking_duration: int,
          turns_per_prof: int,
          rec_prof: int,
          tracking_start: float = None,
          n_profiles: int = 100,
          **kwargs):
    """
    Performs particle tracking using BLonD. For the original implementation
    see the BLonDToTomo.py script.
    """
    rf_params = full_ring.RingAndRFSection_list[0].rf_params
    beam = full_ring.RingAndRFSection_list[0].beam

    volt = rf_params.voltage[0, :]
    eta = ring.eta_0[0][0]
    beta = ring.beta[0][0]
    energy = ring.energy[0][0]
    syncTune = np.sqrt(
        (1 * 1 * volt * np.abs(eta * 1)) / (2 * np.pi * beta ** 2 * energy))
    syncPeriod = ring.t_rev[0] / syncTune

    def store_profile(profile: Profile, save_list: list):

        profile.track()
        save_list.append(profile.n_macroparticles.copy())

    waterfall = []

    n_turns = len(ring.cycle_time[ring.cycle_time <= tracking_duration])

    if tracking_start is not None:
        skip_turns = len(ring.cycle_time[ring.cycle_time
                                         + ring.RingOptions.t_start
                                         <= tracking_start])
    else:
        skip_turns = 0

    if turns_per_prof == -1:
        turns_per_prof = np.round(n_turns - skip_turns) // n_profiles

        log.debug(f'Changed turns_per_prof to {turns_per_prof}')

    if rec_prof == -1:
        rec_prof = n_turns // turns_per_prof - 1
    dt = None
    dE = None

    with time_execution() as t:
        for i in range(n_turns + skip_turns):
            full_ring.track()
            if i < skip_turns:
                continue

            if i % turns_per_prof == 0:
                store_profile(profile, waterfall)
                if len(waterfall) - 1 == rec_prof:
                    dt = beam.dt.copy()
                    dE = beam.dE.copy()

    log.debug(f'Tracking duration: {t.duration}')

    return np.array(waterfall), turns_per_prof, dt, dE


def make_machine(ring: Ring, waterfall: np.ndarray,
                 blondfactory: AbstractBLonDFactory, full_ring: FullRingAndRF,
                 sync_part_x: float, bin_width: float, snpt: int, rec_prof: int,
                 turns_per_prof: int,
                 **kwargs) -> Machine:

    rf_params = full_ring.RingAndRFSection_list[0].rf_params

    n_bins = len(waterfall[0])
    n_profiles = len(waterfall)

    rec_turn = rec_prof * turns_per_prof

    BField = (ring.momentum / (ring.Particle.charge * blondfactory.bending_radius)
              / cont.c).flatten()

    BDot = np.gradient(BField, ring.cycle_time)
    VDot1 = np.gradient(rf_params.voltage[0], ring.cycle_time)
    VDot2 = np.gradient(rf_params.voltage[1], ring.cycle_time)

    phase1 = rf_params.phi_rf_d[0, rec_turn]
    phase2 = rf_params.phi_rf_d[1, rec_turn]

    machine = Machine(turns_per_prof,
                      vrf1=rf_params.voltage[0, rec_turn],
                      vrf2=rf_params.voltage[1, rec_turn],
                      vrf1dot=VDot1[rec_turn],
                      vrf2dot=VDot2[rec_turn],
                      phi12=(phase1 - phase2 + np.pi) / blondfactory.h_ratio,
                      h_ratio=blondfactory.h_ratio,
                      mean_orbit_rad=blondfactory.radius,
                      bending_rad=blondfactory.bending_radius,
                      b0=BField[rec_turn],
                      bdot=BDot[rec_turn],
                      trans_gamma=blondfactory.gamma_transition,
                      rest_energy=ring.Particle.mass,
                      nprofiles=n_profiles,
                      nbins=n_bins,
                      synch_part_x=sync_part_x,
                      dtbin=bin_width,
                      snpt=snpt,
                      )

    machine.values_at_turns()

    if sync_part_x == -1:
        profiles = dprofs.Profiles(machine, 0., waterfall)
        sync_bin, _, _ = pre_process.fit_synch_part_x(profiles)
        machine.synch_part_x = sync_bin

    return machine


def tomo_track(machine: Machine, rec_prof: int):
    """
    Performs particle tracking using the tomography tracking algorithm.
    """

    tracker = Tracking(machine)
    phase_coords, energy_coords = tracker.track(rec_prof)
    xp, yp = parts.physical_to_coords(phase_coords, energy_coords, machine,
                                      tracker.particles.xorigin,
                                      tracker.particles.dEbin)
    xp, yp = parts.ready_for_tomography(xp, yp, machine.nbins)

    return xp, yp


def run_tomo(waterfall: np.ndarray, xp: np.ndarray, yp: np.ndarray,
             machine: Machine, rec_prof: int, n_iter, **kwargs):
    """
    Performs phase space reconstruction using the tomography algorithm.
    """

    waterfall = waterfall.copy()
    tomo = TomographyCpp(waterfall, xp, yp)
    weight = tomo.run(niter=n_iter)

    return tomo, weight


def gradient(f: np.ndarray, dt: np.ndarray) -> np.ndarray:
    """Calculate gradient using forward difference
    First element is zero"""
    out = np.zeros_like(f)

    out[1:] = (f[1:] - f[:-1]) / dt[:-1]
    return out
