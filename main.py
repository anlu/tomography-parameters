#!/usr/bin/env python3
import logging
import time
import warnings
from argparse import ArgumentParser
from datetime import datetime
from os import path
from pprint import pformat
from shutil import copy

import matplotlib.pyplot as plt

import loops
import utils

warnings.simplefilter('ignore')

parser = ArgumentParser()
parser.add_argument('--parameters', type=str, default='parameters.yml',
                    help='Path to file with parameter settings to search.')
sub_parsers = parser.add_subparsers(dest='command')
sim_parser = sub_parsers.add_parser('simulation',
                                    help='Run BLonD simulations and compare'
                                         'results against tomography '
                                         'reconstructions. Requires'
                                         'the hardware to be specified'
                                         'in rf_programs directory.')
sim_parser.add_argument('--accelerator', '-a', type=str, required=True,
                        choices=['psb', 'ps', 'sps', 'lhc'],
                        help='For which CERN accelerator to import BLonD'
                             'objects for.')
sim_parser.add_argument('--cycle', '-c', type=str, required=True)
sim_parser.add_argument('--timeframe', '-t', type=str, required=True)

dat_parser = sub_parsers.add_parser('dat',
                                    help='Run only tomography reconstruction, '
                                         'the only available metric will be'
                                         'the discrepancy returned by the'
                                         'tomography routine.')
dat_parser.add_argument('dat', type=str,
                        help='Path to a dat file containing the machine'
                             'parameters')

args = parser.parse_args()

# set up logger first

timestamp = datetime.now().strftime('%Y%m%d_%H%M')

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter(
    '%(asctime)s - [%(levelname)s] - %(name)s - %(message)s',
    "%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)

log.addHandler(ch)

# write log to file
if args.command == 'simulation':
    log_dir = path.join('logs', '{}_{}_{}_{}'.format(
        timestamp, args.accelerator.upper(),
        args.cycle.upper(), args.timeframe.upper(),
    ))
elif args.command == 'dat':
    log_dir = path.join('logs', '{}_{}'.format(
        timestamp, path.splitext(path.basename(args.dat))[0]
    ))
else:
    raise NotImplementedError()
utils.mkdir_p(log_dir)

fh = logging.FileHandler(path.join(log_dir, 'log.log'))
fh.setLevel(logging.DEBUG)

fh.setFormatter(formatter)
log.addHandler(fh)

log.info(f'Using timestamp {timestamp} for logging')

if args.command == 'simulation':
    with utils.time_execution() as t:
        results = loops.simulate_and_reconstruct(args, log_dir)
elif args.command == 'dat':
    with utils.time_execution() as t:
        results = loops.reconstruct_only(args)
else:
    raise NotImplementedError(f'Command {args.command} is not implemented.')

# Plot runtime vs discrepancy
utils.show_runtime_vs_discrepancy(results)
plt.savefig(path.join(log_dir, 'runtime_vs_discrepancy.eps'), format='eps')
plt.clf()

results.to_pickle(path.join(log_dir, 'results.pkl'))
copy(args.parameters, path.join(log_dir, 'parameters.yml'))

log.info('=' * 40)
log.info('FINAL RESULTS')
log.info('\n' + pformat(results))
log.info('=' * 40)

end = time.time()

log.info(f'Grid search time: {t.duration / 60} minutes')
