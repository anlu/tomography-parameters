from typing import Any, Sequence

from yaml import FullLoader, load
import numpy as np
import itertools, functools, operator

tracking_parameters = {
    'turns_per_prof',
    'n_slices',
    'initial_time',
    'final_time',
    'tracking_duration',
    'n_macro',
    'rec_prof',
    'n_profiles'
}

tomo_parameters = {
    'snpt',
    'n_iter',
    'rebin',
    'rec_prof',
    'sync_part_x',
    'cut_left',
    'cut_right',
    'voltage',
}

# set these parameters to -1 if not passed
automatic_parameters = {
    'turns_per_prof',
    # 'n_slices',
    'rec_prof',
    'sync_part_x',
    'cut_left',
    'cut_right',
}


class Parameters:
    """
    A convenience class to represent the parameter space in the grid search.

    Encapsulates the parameters and provides the method
    :py:meth:`gen_parameters` to generate the parameter space as we go to
    conserve memory.
    """
    def __init__(self):
        self.config = None

        # The full grid, and only the varied parameters, respectively
        self.grid = None
        self.grid_parameters = None

    def load_config(self, path: str) -> dict:
        """
        Loads a YAML formatted file from disk.
        Saves the dictionary in the object.

        Parameters
        ----------
        path : str
            Absolute or relative path to the YAML file.

        Returns
        -------
        dict :
            The loaded yaml file.
        """

        with open(path, 'r') as f:
            config = load(f, Loader=FullLoader)

        def replace_pi(config: dict):
            # TODO add a string parser to support factors of pi
            for k, v in config.items():
                if isinstance(v, dict):
                    replace_pi(v)
                elif isinstance(v, str) and v == 'pi':
                    config[k] = np.pi

        replace_pi(config)

        self.config = config

        return config

    def create_grid(self):
        """
        Pre-processes the parameter grid, but does not create parameter
        grid instances. I.e. creates the parameter space axes, but does not
        populate the grid. This is done instead in the
        :py:meth:`gen_parameters` method.

        Raises
        ------
        ValueError :
            If a parameter is not in a supported format.

        """

        tracking_grid = Grid()
        tomo_grid = Grid()

        for param_n, param_v in self.config['parameters'].items():
            if isinstance(param_v, dict):
                if not is_variable(param_v):
                    raise ValueError(f'Malformatted parameter {param_n}')

                lower = param_v['lower']
                upper = param_v['upper']

                if isinstance(lower, int) and isinstance(upper, int):
                    val = np.linspace(lower, upper, upper - lower + 1,
                                      dtype=int)
                else:
                    if 'n_points' in param_v:
                        val = np.linspace(lower, upper,
                                          param_v['n_points'],
                                          dtype=float)
                    else:
                        val = np.linspace(lower, upper, dtype=float)

                param_v = val.tolist()
            elif isinstance(param_v, list):
                pass
            elif isinstance(param_v, int) or isinstance(param_v, float):
                param_v = [param_v]
            else:
                raise ValueError(f'Unrecognized type {type(param_v)} '
                                 f'for param {param_n}')

            if param_n in tracking_parameters:
                tracking_grid.register_param(param_n, param_v)
            if param_n in tomo_parameters:
                tomo_grid.register_param(param_n, param_v)

        for param in automatic_parameters:
            if param in tracking_parameters:
                if param not in tracking_grid.grid:
                    tracking_grid.register_param(param, [-1])
            elif param in tomo_parameters:
                if param not in tomo_grid.grid:
                    tomo_grid.register_param(param, [-1])

        return tracking_grid, tomo_grid

    def is_fixed_tracking(self) -> bool:
        """
        Determine if the tracking parameters are fixed (that is, not part
        of the grid search). This could in theory enable tracking once,
        and only running a grid search on the tomography part of the code.

        Returns
        -------
        bool :
            True if tracking parameters are fixed (non-variable), else
            False
        """

        for param_n, param_v in self.config['parameters'].items():
            if param_n in tracking_parameters and is_variable(param_v):
                return False

        return True


def is_variable(param_dict: Any) -> bool:
    """
    Tests if the input parameter is a dictionary, in which case it will test
    if the dictionary contains upper and lower bounds for a parameter. This
    indicates that the parameter is variable (and should be part of the grid
    search).

    Parameters
    ----------
    param_dict : Any
        A dictionary, or a numeric value.

    Returns
    -------
    bool :
        True if the input parameters is variable, else False

    """
    if ((isinstance(param_dict,
                  dict) and 'lower' in param_dict and 'upper' in param_dict)\
            or isinstance(param_dict, list))\
            and len(param_dict) > 1:
        return True
    return False


class Grid:
    def __init__(self):
        self.grid = dict()
        self.grid_parameters = dict()

    def register_param(self, param_n: str, param_v: Sequence):
        self.grid[param_n] = param_v
        if len(param_v) > 1:
            self.grid_parameters[param_n] = param_v

    def __len__(self):
        prod = 1

        for k, v in self.grid_parameters.items():
            prod *= len(v)

        return prod

    def __iter__(self):
        # return itertools.product(*self.grid.values())
        keys = self.grid.keys()
        values = self.grid.values()

        for instance in itertools.product(*values):
            yield dict(zip(keys, instance))

    def get_varied_params(self, instance: dict) -> dict:
        """
        Strips non-varied parameters from an instance of the parameter / grid
        space to conserve memory and enable a better overview of which
        parameters were changed.

        Parameters
        ----------
        instance : dict
            A dictionary containing all the instance parameters, each key
            should correspond to a parameter.

        Returns
        -------
        dict :
            A dictionary containing only the varied parameters. The keys of the
            dictionary corresponds to the names of the parameters.
        """
        output = dict()

        for param_n in self.grid_parameters.keys():
            output[param_n] = instance[param_n]

        return output
