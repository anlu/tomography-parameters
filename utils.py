import itertools
import logging
from datetime import datetime
from numbers import Number
from os import makedirs, path
from time import time
from typing import Tuple, Sequence, Union

import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib.gridspec as gSpec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from blond.beam.beam import Beam
from blond.input_parameters.rf_parameters import RFStation
from blond.input_parameters.ring import Ring
from matplotlib.font_manager import FontProperties

from annotatefinder import AnnoteFinder


class time_execution:
    """
    Convenience class for timing execution. Used simply as
    >>> with time_execution() as t:
    >>>     # some code to time
    >>> print(t.duration)
    """

    def __init__(self):
        self.start = 0
        self.end = 0
        self.duration = 0

    def __enter__(self):
        self.start = time()

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.end = time()
        self.duration = self.end - self.start


def bin_simulated(t_range: np.ndarray, E_range: np.ndarray, dt: np.ndarray,
                  dE: np.ndarray, rf_params: RFStation, ring: Ring,
                  rec_turn: int):
    """
    Bins continuous simulated data to discrete histograms.

    Parameters
    ----------
    t_range : np.ndarray
        The time span to bin to
    E_range : np.ndarray
        The energy span to bin to
    ring : Ring
        A BLonD Ring object

    Returns
    -------
    np.ndarray :
        A 2D ndarray containing the histogram
    """
    tWidth = np.diff(t_range[:2])[0]
    histT = np.linspace(t_range[0] - tWidth / 2, t_range[-1] + tWidth / 2,
                        len(t_range) + 1)
    EWidth = np.diff(E_range[:2])[0]
    histE = np.linspace(E_range[0] - EWidth / 2, E_range[-1] + EWidth / 2,
                        len(E_range) + 1)

    sync_phase_t = (np.pi + np.arcsin(
        ring.delta_E[0, rec_turn] / rf_params.voltage[0, rec_turn])) / (
                           2 * np.pi) * ring.t_rev[rec_turn]

    psHist = np.histogram2d(dt - sync_phase_t, dE,
                            bins=[histT, histE])

    return psHist[0]


def mkdir_p(dir: str):
    try:
        makedirs(dir, exist_ok=True)
    except OSError:
        pass


def make_param_string(instance: dict) -> str:
    """
    Convert the varied parameters to a human readable string

    Parameters
    ----------
    instance : dict
        A dictionary containing only the varied parameters. Each key
        corresponds to a parameter name.

    Returns
    -------
    str :
        A human readable string
    """
    out = ''

    for param_n, param_v in instance.items():
        out += f'{param_n}={param_v}\n'

    return out.strip()


def show_waterfall(waterfall: np.ndarray):
    plt.pcolor(waterfall)
    plt.show()


def show_phase_space(t_range: np.ndarray, E_range: np.ndarray,
                     tomo_space: np.ndarray, beam: Beam, ring: Ring,
                     waterfall: np.ndarray, discrepancy: np.ndarray,
                     rec_prof: int, subtitle: str = ''):
    energy_proj = np.sum(tomo_space, axis=0)
    energy_proj /= np.sum(energy_proj)

    time_proj = np.sum(tomo_space, axis=1)
    time_proj /= np.sum(time_proj)

    fig = plt.figure(1)

    gs = gSpec.GridSpec(4, 4)

    ax1 = plt.subplot(gs[1:, :3])
    ax2 = plt.subplot(gs[0, :3])
    ax3 = plt.subplot(gs[1:, 3])
    ax4 = plt.subplot(gs[0, 3])

    ax1.contourf(t_range * 1E9, E_range / 1E6, tomo_space.T, levels=30,
                 cmap='Oranges')

    ax1.scatter((beam.dt[::100] - ring.t_rev[0] / 2) * 1E9,
                beam.dE[::100] / 1E6,
                alpha=0.5)

    ax1.set_xlabel("Time (ns)", fontsize=12)
    ax1.set_ylabel("Energy (MeV)", fontsize=12)

    ax2.plot(waterfall[rec_prof] / np.sum(waterfall[rec_prof]),
             color='black')
    ax2.plot(time_proj / np.sum(time_proj),
             color='red')

    for ax in (ax2, ax3, ax4):
        ax.set_xticks([])
        ax.set_yticks([])

    ax3.plot(energy_proj, range(len(energy_proj)), color='red')
    ax4.plot(discrepancy)

    plt.gcf().set_size_inches(7, 6)
    if subtitle != '':
        plt.suptitle('Reconstruction\n' + subtitle)
    plt.tight_layout()


def show_diff(tomo_space: np.ndarray, simulated_space: np.ndarray,
              t_range: np.ndarray, E_range: np.ndarray,
              subtitle: str = ''):
    norm_tomo = tomo_space / np.sum(tomo_space)
    norm_blond = simulated_space / np.sum(simulated_space)
    delta = norm_tomo - norm_blond

    delta_min = np.min(delta)
    delta_max = np.max(delta)
    delta_lim = np.max([np.abs(delta_min), np.abs(delta_max)])

    true_max = np.max([np.max(norm_tomo), np.max(norm_blond)])
    true_min = np.min([np.min(norm_tomo[norm_tomo != 0]),
                       np.min(norm_blond[norm_blond != 0])])

    f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(30, 10))
    ax1.contourf(t_range * 1E9, E_range / 1E6, norm_tomo.T, cmap='copper',
                 levels=np.linspace(true_min, true_max, 100))
    ax2.contourf(t_range * 1E9, E_range / 1E6, norm_blond.T, cmap='copper',
                 levels=np.linspace(true_min, true_max, 100))
    m = ax3.contourf(t_range * 1E9, E_range / 1E6, delta.T, cmap='seismic',
                     levels=np.linspace(-delta_max, delta_max, 100))

    if subtitle != '':
        plt.suptitle(f'Difference plot\n{subtitle}')
    plt.tight_layout()


def show_runtime_vs_discrepancy(df: pd.DataFrame, interactive: bool = False,
                                max_time: float = None,
                                max_discrepancy: float = None,
                                color_groups: list = None,
                                title: str = None,
                                metric: str = 'discrepancy',
                                scatter: list = None):
    if scatter is not None:
        for i, metric in enumerate(scatter):
            if f'diff_{metric}' not in df:
                raise ValueError(
                    f'Metric {metric} is not available in the dataframe')
            scatter[i] = f'diff_{metric}'
    else:
        if f'diff_{metric}' not in df:
            raise ValueError(f'Metric {metric} is not available in the dataframe')
        orig_metric = metric
        metric = f'diff_{metric}'

    # first reconstruct the labels
    cols_exclude = {'index', 'time',
                    'diff_discrepancy',
                    'diff_phase_space',
                    'diff_time_proj',
                    'diff_emittance_rms',
                    'diff_emittance_90',
                    'diff_rms_dpp',
                    'label'}

    param_cols = [param for param in list(df) if param not in cols_exclude]
    for index, row in df.iterrows():
        label = ''
        for col in param_cols:
            label += f'{col}={row[col]}\n'
        label = label.strip()
        df.loc[index, 'label'] = label

    n_points = len(df['time'])
    fontP = FontProperties()
    fontP.set_size('xx-small')

    fig, ax = plt.subplots()

    xtol = max_time / 100 if max_time is not None else df['time'].max() / 100
    if scatter is not None:
        upper = np.max((df[scatter[0]], df[scatter[1]]))
    else:
        upper = df[metric].max()
    ytol = max_discrepancy / 100 if max_discrepancy is not None else upper / 100

    # if we're grouping parameters
    if color_groups is not None and len(color_groups) > 0:
        combinations = list()

        for param in color_groups:
            if param not in df:
                raise ValueError(f'The parameter {param} does not exist in the'
                                 f'dataframe')
            else:
                combinations.append(pd.unique(df[param]))

        df['group'] = -1

        for i, combination in enumerate(itertools.product(*combinations)):
            combination = dict(zip(color_groups, combination))
            query = ' & '.join(
                [f'{k}=={v}' for k, v in combination.items()])
            rows = df.query(query)
            df.at[rows.index, 'group'] = i

        n_colors = i + 1

        colormap = cm.hsv

        if n_colors <= 20:
            colormap = cm.tab20
        elif n_colors <= 10:
            colormap = cm.tab10

        colorlist = [colors.rgb2hex(colormap(i)) for i in
                     np.linspace(0, 1.0, n_colors)]

        for col in range(n_colors):
            group = df.loc[df['group'] == col]
            if group.size == 0:
                continue
            if scatter is not None:
                ax.scatter(group[scatter[0]], group[scatter[1]],
                           c=colorlist[col], label=group['label'].iloc[0])
            else:
                ax.scatter(group['time'], group[metric], c=colorlist[col],
                           label=group['label'].iloc[0])

        print(f'Number of groups: {n_colors}')

        if n_colors < 20:
            plt.subplots_adjust(right=0.8)
            ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left', prop=fontP)
    else:
        if n_points > 20:
            ax.scatter(df['time'], df[metric])
        else:
            colormap = cm.hsv
            colorlist = [colors.rgb2hex(colormap(i)) for i in
                         np.linspace(0, 0.9, n_points)]

            for i, c in enumerate(colorlist):
                x = df['time'][i]
                y = df[metric][i]
                label = df['label'][i]
                ax.scatter(x, y, c=c, label=label)

            if len(df['time']) < 20:
                plt.subplots_adjust(right=0.8)
                ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left',
                          prop=fontP)

    if interactive:
        if scatter is not None:
            af = AnnoteFinder(df[scatter[0]], df[scatter[1]], df['label'],
                              xtol=xtol,
                              ytol=ytol)
        else:
            af = AnnoteFinder(df['time'], df[metric], df['label'],
                              xtol=xtol,
                              ytol=ytol)
        fig.canvas.mpl_connect('button_press_event', af)

    if scatter is None:
        if 'emittance' in metric or 'dpp' in metric:
            ax.hlines(1, xmin=-10, xmax=df['time'].max() * 2)
            true_max = np.max((df[metric].max(), df[metric].min())) - 1
            ylim = 1.2 * true_max
            ax.set_ylim(1-ylim, 1+ylim)
            ax.set_xlim(0, df['time'].max() * 1.2)

    if scatter is not None:
        ax.set_xlabel(f'Discrepancy {scatter[0].replace("diff_", "")}')
        ax.set_ylabel(f'Discrepancy {scatter[1].replace("diff_", "")}')
        suptitle = ''
    else:
        ax.set_xlabel('Run time (s)')
        ax.set_ylabel(f'Discrepancy ({orig_metric})')

        suptitle = f'Runtime vs discrepancy ({orig_metric})'
        if title is not None:
            suptitle += f'\n{title}'

    fig.suptitle(suptitle, fontsize=16)

    if max_time:
        ax.set_xlim(0, max_time)
    if max_discrepancy:
        ax.set_ylim(0, max_discrepancy)

    return fig


def filter_frequencies(amplitude: np.ndarray,
                       threshold: float = 0.01, invert: bool = False):
    if invert:
        indices = np.abs(amplitude) > threshold
    else:
        indices = np.abs(amplitude) < threshold

    filtered_amplitude = amplitude.copy()
    filtered_amplitude[indices] = 0

    print(f'# removed frequencies: {np.sum(indices)} / {amplitude.size}')

    return filtered_amplitude


def show_mountainrange(waterfall: Union[np.ndarray, Sequence[np.ndarray]],
                       l_cut: float = None, r_cut: float = None,
                       t_rev: float = None, step_size=None, ax=None):
    n_slices = len(waterfall[0])
    n_profiles = len(waterfall)

    if step_size is None:
        step_size = np.max(waterfall) / 50

    if ax is None:
        fig = plt.figure(frameon=False)
        ax = fig.add_subplot(111)
    else:
        fig = None
    rng = np.arange(n_profiles).reshape(n_profiles, 1) * step_size

    if t_rev is not None:
        xax = np.arange(n_slices) / n_slices * t_rev * 1e9
        l_cut *= t_rev / n_slices * 1e9
        r_cut *= t_rev / n_slices * 1e9
        ax.set_xlabel('(ns)')
    else:
        xax = np.arange(n_slices)

    ax.plot(xax, (waterfall + rng).T, color='black')

    if l_cut is not None:
        ax.axvline(x=l_cut)
    if r_cut is not None:
        ax.axvline(x=r_cut)

    ax.set_yticks([])

    if fig is not None:
        plt.show()


def get_cuts(waterfall: Sequence, threshold: int = None, margin: int = 20) -> \
        Tuple[Union[float, np.ndarray], Union[float, np.ndarray]]:
    if isinstance(waterfall, tuple) or isinstance(waterfall, list):
        waterfall = np.array(waterfall).real

    n_bins = len(waterfall[0])
    diff = np.sum(np.abs(waterfall[:, 1:] - waterfall[:, :-1]), axis=0)

    if threshold is None:
        threshold = (diff.max() - diff.min()) / 5

    # filters out noise
    actual_waterfall = diff > threshold

    cut_left = np.argmax(actual_waterfall)
    cut_right = n_bins \
        - np.argmax(actual_waterfall[::-1]) + 1

    if cut_left > margin:
        cut_left -= margin
    else:
        cut_left = 0

    if n_bins - cut_right > margin:
        cut_right += margin
    else:
        cut_right = n_bins

    return int(cut_left), int(cut_right)


def cut_waterfall(waterfall: Sequence, cut_left: Number, cut_right: Number) \
        -> np.ndarray:

    if not isinstance(waterfall, np.ndarray):
        waterfall = np.array(waterfall)

    # if cuts are in time, convert to bin numbers
    if isinstance(cut_left, float):
        cut_left = int(cut_left)
    if isinstance(cut_right, float):
        cut_right = int(cut_right)

    waterfall = waterfall[:, cut_left:cut_right]

    return waterfall


def filter_particles(xp: np.ndarray, yp: np.ndarray,
                     cut_left: int, cut_right: int):

    invalid_pts = np.argwhere(
        np.logical_or(xp < cut_left, xp >= cut_right))

    if np.size(invalid_pts) > 0:
        # Mark particle as invalid only once
        invalid_pts = np.unique(invalid_pts.T[0])
        # Save number of invalid particles
        nr_lost_pts = len(invalid_pts)
        # Remove invalid particles
        xp = np.delete(xp, invalid_pts, axis=0)
        yp = np.delete(yp, invalid_pts, axis=0)

    xp -= cut_left
    yp -= cut_left

    return xp, yp


def filter_profiles(waterfall: np.ndarray):
    waterfall = waterfall / waterfall.sum()

    bin_sum = waterfall.sum(axis=1)

    threshold = bin_sum.max() - bin_sum.min() - bin_sum.mean()
    good_frames = bin_sum > threshold

    good_waterfall = waterfall[good_frames, :]

    return good_waterfall
