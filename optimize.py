from copy import deepcopy

from numpy import fft
from tomo.data import pre_process
from tomo.tracking import Machine
from tomo.tracking import tracking
from tomo.tomography import tomography
from tomo.tracking import particles as parts
from tomo import shortcuts
from scipy import optimize as opt
import numpy as np
import utils
import pandas as pd

import logging

log = logging.getLogger(__name__)

__all__ = ['optimize_volt_syncbin']


def optimize_volt_syncbin(machine: Machine, profiles, rec_prof, n_iter,
                          opt_v2: bool = True):
    volt1_magnitude = np.floor(np.log10(np.abs(machine.vrf1)))
    volt1_magnitude -= 1

    if machine.vrf2 == 0. or not opt_v2:
        v2 = False
    else:
        v2 = True
        volt2_magnitude = np.floor(np.log10(np.abs(machine.vrf2)))
        volt2_magnitude -= 1

        phi_magnitude = np.floor(np.log10(np.abs(machine.phi12)))
        #phi_magnitude += 1

    if machine.synch_part_x == -1:
        sync_bin, _, _ = pre_process.fit_synch_part_x(profiles)
        machine.synch_part_x = sync_bin

    # sync_bin_magnitude = np.floor(np.log10(np.abs(machine.synch_part_x)))
    sync_bin_magnitude = 0

    # define optimization function handle
    def opt_func(optArgs, *args):
        m = deepcopy(machine)
        syncBin = optArgs[0] * np.power(10, sync_bin_magnitude)
        voltage = np.power(10, volt1_magnitude) * optArgs[1]

        m.vrf1 = voltage
        m.synch_part_x = syncBin

        if v2:
            voltage2 = np.power(10, volt2_magnitude) * optArgs[2]
            phi12 = np.power(10, phi_magnitude) * optArgs[3]
            m.vrf2 = voltage2
            m.phi12 = phi12

        m.values_at_turns()

        xp, yp = shortcuts.track(m, rec_prof)
        tomo = shortcuts.tomogram(profiles.waterfall, xp, yp, n_iter)

        log_str = f'sync_bin: {syncBin}, volt1: {voltage}, '
        if v2:
            log_str += f'volt2: {voltage2}, phi12: {phi12}, '
        log_str += f'discr: {tomo.diff[-1]}'
        log.info(log_str)

        # blow up the discrepancy because it's too small
        return tomo.diff[-1] * 1e2

    # optimize function with scipy
    # voltage is scaled down by 1e4 because minimize doesn't work well
    # for large numbers
    opt_x0 = [machine.synch_part_x * np.power(10, -sync_bin_magnitude),
              machine.vrf1 * np.power(10, -volt1_magnitude)]
    if v2:
        opt_x0 += [machine.vrf2 * np.power(10, -volt2_magnitude),
                   machine.phi12 * np.power(10, -phi_magnitude)]
    res = opt.minimize(opt_func, opt_x0, method='Nelder-Mead',
                       options={'disp': True})

    opt_sync_bin = res.x[0] * np.power(10, sync_bin_magnitude)
    opt_volt1 = res.x[1] * np.power(10, volt1_magnitude)

    if v2:
        opt_volt2 = res.x[2] * np.power(10, volt2_magnitude)
        opt_phi12 = res.x[3] * np.power(10, phi_magnitude)
    else:
        opt_volt2 = machine.vrf2
        opt_phi12 = machine.phi12

    log_str = f'Optimized parameters: volt1={opt_volt1}, volt2={opt_volt2}, ' \
              f'phi12={opt_phi12}, sync_bin={opt_sync_bin}, discr={0}'
    log.info(log_str)

    return opt_sync_bin, opt_volt1, opt_volt2, opt_phi12


def optimize_cuts(machine: Machine, waterfall, rec_prof: int, max_margin=20,
                  n_iter=20, return_results=False):
    ft = fft.fft(waterfall)
    filtered = utils.filter_frequencies(ft, threshold=ft.max()/100)
    ift = fft.ifft(filtered)
    source_waterfall = waterfall.copy()

    source_xp, source_yp = shortcuts.track(machine, rec_prof)
    results = []

    for margin in range(max_margin, -1, -1):
        m = deepcopy(machine)
        waterfall = source_waterfall.copy()
        xp, yp = source_xp.copy(), source_yp.copy()

        with utils.time_execution() as filter_time:
            cut_left, cut_right = utils.get_cuts(ift, margin=margin)

            xp, yp = utils.filter_particles(xp, yp, cut_left, cut_right)

            waterfall = utils.cut_waterfall(waterfall, cut_left, cut_right)
        # utils.show_mountainrange(source_waterfall, cut_left, cut_right)

        m.synch_part_x -= cut_left
        n_bins = waterfall.shape[1]
        m.nbins = n_bins

        with utils.time_execution() as tomo_time:
            tomo = shortcuts.tomogram(waterfall,
                                      xp, yp, n_iter=n_iter)

        results.append({'time': tomo_time.duration,
                        'margin': margin,
                        'filter_time': filter_time.duration,
                        'discr': tomo.diff[-1]})

    df = pd.DataFrame(results)

    min_idx = df['discr'].idxmin()
    opt_margin = df['margin'][min_idx]

    cut_left, cut_right = utils.get_cuts(ift, margin=opt_margin)

    if return_results:
        return cut_left, cut_right, results
    else:
        return cut_left, cut_right
