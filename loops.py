import importlib
import logging
from argparse import Namespace
from os import path
from pprint import pformat

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tomo.data.data_treatment as treat
import tomo.utils.tomo_input as tomoin
import yaml
from tomo.data.data_treatment import rebin, phase_space as make_phase_space

from tomo.data import post_process
import utils
from parameters import Parameters
from tomography import track, make_machine, tomo_track, run_tomo

log = logging.getLogger(__name__)


def simulate_and_reconstruct(args: Namespace, log_dir: str = '') \
        -> pd.DataFrame:
    # paths to ring BLonD factories and voltage/phase/momentum programs
    with open('rf_programs/rings.yml', 'r') as f:
        experiments = yaml.full_load(f)

    # argument checking
    if args.accelerator.upper() not in experiments:
        raise ValueError(f'Accelerator {args.accelerator} does not exist in'
                         f'rf_programs/rings.yml')
    if args.cycle.upper() not in experiments[args.accelerator.upper()]:
        raise ValueError(f'Cycle {args.cycle} does not exist for accelerator'
                         f'{args.accelerator} in rf_programs/rings.yml')

    cycle = experiments[args.accelerator.upper()][args.cycle.upper()]

    # dynamically import correct BLonD factory
    blondfactory = importlib.import_module(
        cycle['blondfactory'].replace('/', '.'))

    # read default beam parameter from file
    with open(path.join('rf_programs', args.accelerator,
                        'beam_parameters.yml')) as f:
        default_beam_params = yaml.full_load(f)

    timeframe = f'{args.cycle.upper()}_{args.timeframe}'
    if timeframe not in default_beam_params:
        raise ValueError(f'Time frame {args.cycle.upper()}_{args.timeframe} '
                         f'does not exist in the beam parameter file.')
    default_beam_params = default_beam_params[timeframe]

    # Load parameters first in order to create logging directory
    parameters = Parameters()

    config = parameters.load_config(args.parameters)
    options = config['options']

    # create grid
    tracking_grid, tomo_grid = parameters.create_grid()

    results = []

    log.info(f'Starting grid search for ring {args.accelerator.upper()}, '
             f'cycle {timeframe}')

    for i, tracking_instance in enumerate(tracking_grid):
        # override default beam parameters with grid
        for param, val in default_beam_params.items():
            if param not in tracking_instance:
                tracking_instance[param] = val
            else:
                log.debug('Default beam parameter {}:{} has been overridden '
                          'by the grid {}:{}'.format(
                    param, val,
                    param, tracking_instance[param]
                ))
        if 'tracking_start' not in tracking_instance:
            tracking_instance['tracking_start'] = None

        factory = blondfactory.BLonDFactory(cycle, **tracking_instance)

        profile, beam, ring, full_ring = factory.get_ring()

        # print(f'n_slices: {profile.cut_options.n_slices}')
        # import sys
        # sys.exit(0)

        (source_waterfall,
         turns_per_prof,
         dt, dE) = track(profile, ring, full_ring,
                         tracking_duration=tracking_instance['tracking_duration'],
                         turns_per_prof=tracking_instance['turns_per_prof'],
                         n_profiles=tracking_instance['n_profiles'],
                         rec_prof=tracking_instance['rec_prof'],
                         tracking_start=tracking_instance['tracking_start'])

        # update turns_per_prof if it was updated by the tracking
        tracking_instance['turns_per_prof'] = turns_per_prof

        utils.show_mountainrange(source_waterfall)

        for instance in tomo_grid:
            params = {**tomo_grid.get_varied_params(instance),
                      **tracking_grid.get_varied_params(tracking_instance)}
            instance['turns_per_prof'] = turns_per_prof

            log.debug(f'Processing parameters {pformat(params)}')

            waterfall = source_waterfall.copy()

            # cut waterfall
            if ('cut_left' not in instance and 'cut_right' not in instance) \
                    or (instance['cut_left'] == -1 and instance[
                'cut_right'] == -1):
                cut_left, cut_right = utils.get_cuts(waterfall, margin=20)
                instance['cut_left'] = cut_left
                instance['cut_right'] = cut_right

            waterfall = utils.cut_waterfall(
                waterfall, instance['cut_left'], instance['cut_right'])

            # Rebin data
            bin_width = profile.bin_centers[1] - profile.bin_centers[0]
            (waterfall,
             instance['bin_width']) = rebin(waterfall, instance['rebin'],
                                            bin_width)

            # set reconstruction profile to last one if it is unset
            if 'rec_prof' not in instance or instance['rec_prof'] == -1:
                instance['rec_prof'] = len(waterfall) - 1

            machine = make_machine(ring, waterfall, factory,
                                   full_ring,
                                   **instance)

            # Tomography tracking routine
            with utils.time_execution() as t:
                xp, yp = tomo_track(machine, instance['rec_prof'])

            log.debug(f'Tomo tracking took {t.duration:.2f}s')

            # Reconstruction routine
            with utils.time_execution() as t:
                tomo, weight = run_tomo(waterfall, xp, yp, machine,
                                        instance['rec_prof'],
                                        instance['n_iter'])
            log.debug(f'Reconstruction took {t.duration:.4f}s')
            tomo_time = t.duration

            (t_range,
             E_range,
             phase_space) = make_phase_space(tomo, machine,
                                             instance['rec_prof'])
            discrepancy = tomo.diff.copy()

            rec_turn = instance['rec_prof'] * turns_per_prof

            # Bin continuous data
            simulated_bins = utils.bin_simulated(t_range, E_range, dt, dE,
                                                 full_ring.RingAndRFSection_list[
                                                     0].rf_params,
                                                 ring, rec_turn)

            phase_space = phase_space.clip(0.0)
            simulated_bins = simulated_bins.clip(0.0)

            norm_phase_space = phase_space / phase_space.sum()
            norm_simulated_space = simulated_bins / simulated_bins.sum()

            # calculate difference metrics
            phase_space_diff = np.linalg.norm(norm_phase_space
                                              - norm_simulated_space,
                                              ord=2)
            time_space_diff = np.linalg.norm(
                np.sum(norm_phase_space, axis=1) - np.sum(norm_simulated_space,
                                                          axis=1),
                ord=2)

            reference_rms_emittance = post_process.emittance_rms(
                np.std(dt), np.std(dE))
            reconstructed_rms_emittance = post_process.emittance_rms(
                phase_space, t_range, E_range
            )

            emittance_rms_diff = post_process.emittance_rms(
                phase_space, t_range, E_range) / \
                                 post_process.emittance_rms(simulated_bins,
                                                            t_range, E_range)

            emittance_90_diff = post_process.emittance_fractional(
                phase_space, t_range, E_range) / \
                                post_process.emittance_fractional(
                                    simulated_bins, t_range,
                                    E_range)

            momentum = ring.momentum[0, rec_turn]
            energy = ring.energy[0, rec_turn]
            mass = ring.Particle.mass

            rms_dpp_diff = post_process.rms_dpp(phase_space.sum(0), E_range,
                                                energy, momentum, mass) / \
                post_process.rms_dpp(simulated_bins.sum(0), E_range,
                                     energy, momentum, mass)

            # do not log result if above limit
            if 'constraints' in options:
                if tomo_time > options['constraints']['time']:
                    continue

            # create output and add to results
            output = {
                **params,
                'time': tomo_time,
                'diff_discrepancy': tomo.diff[-1],
                'diff_phase_space': phase_space_diff,
                'diff_time_proj': time_space_diff,
                'diff_emittance_rms': emittance_rms_diff,
                'diff_emittance_90': emittance_90_diff,
                'diff_rms_dpp': rms_dpp_diff,
                # 'diff_emittance_rms': reference_rms_emittance / reconstructed_rms_emittance
                # 'n_profiles': len(source_waterfall),
                # 'n_bins': instance['cut_right'] - instance['cut_left']
            }
            results.append(output)

            # Produce reconstruction and comparison plots, saved to eps files
            if options['plot']:
                param_string = utils.make_param_string(params)

                utils.show_phase_space(t_range, E_range, simulated_bins, beam,
                                       ring,
                                       waterfall, discrepancy,
                                       instance['rec_prof'],
                                       param_string)
                plt.savefig(path.join(log_dir, 'reconstruction_{}.eps'.format(
                    param_string.replace('\n', '')
                )), format='eps')
                plt.clf()

                utils.show_diff(phase_space, simulated_bins, t_range, E_range,
                                param_string)
                plt.savefig(path.join(log_dir, 'comparison_{}.eps'.format(
                    param_string.replace('\n', '')
                )))
                plt.clf()
                plt.close()

            log.info('Processed parameters\n{}'.format(pformat(output)))

    # Save grid search results
    results = pd.DataFrame(results)
    return results


def reconstruct_only(args: Namespace) -> pd.DataFrame:
    parameters = Parameters()

    config = parameters.load_config(args.parameters)

    _, tomo_grid = parameters.create_grid()

    results = []

    for instance in tomo_grid:
        params = tomo_grid.get_varied_params(instance)
        log.debug(f'Processing parameters {pformat(params)}')

        with open(args.dat, 'r') as f:
            raw_params, raw_data = tomoin._split_input(f.readlines())

        machine, frames = tomoin.txt_input_to_machine(raw_params)

        # insert grid parameters
        if 'snpt' in instance:
            machine.snpt = instance['snpt']

        if 'cut_left' in instance and 'cut_right' in instance and \
            (instance['cut_left'] != -1 and instance['cut_right'] != -1):
            frames.skip_bins_start = instance['cut_left']
            frames.skip_bins_end = instance['cut_right']

        if 'rebin' in instance:
            frames.rebin = instance['rebin']

        if instance['rec_prof'] is None:
            instance['rec_prof'] = machine.filmstart

        if 'voltage' in instance:
            machine.vrf1 = instance['voltage']

        machine.values_at_turns()
        waterfall = frames.to_waterfall(raw_data)  # waterfall cutting

        # rebinning
        profiles = tomoin.raw_data_to_profiles(
            waterfall, machine, frames.rebin, frames.sampling_time)

        # fit synch_part_x
        if 'sync_part_x' in instance and instance['sync_part_x'] != -1:
            machine.synch_part_x = instance['sync_part_x']
        elif machine.synch_part_x < 0:
            sync_bin, _, _ = treat.fit_synch_part_x(profiles)
            machine.synch_part_x = sync_bin

        with utils.time_execution() as t:
            xp, yp = tomo_track(machine, instance['rec_prof'])

        with utils.time_execution() as t:
            tomo, weight = run_tomo(profiles.waterfall, xp, yp, machine,
                                    instance['rec_prof'],
                                    instance['n_iter'])
        log.debug(f'Reconstruction took {t.duration:.4f}s')
        tomo_time = t.duration

        discrepancy = tomo.diff.copy()

        output = {**params,
                  'time': tomo_time,
                  'diff_discrepancy': discrepancy[-1]}

        results.append(output)

    return pd.DataFrame(results)
