"""
Find optimal parameters from a grid search by calculating the sum of squares
from selectable metrics (lower is better), then sorted by value to show
the best 10 parameter settings subject to a time constraint.
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from argparse import ArgumentParser


def main():
    parser = ArgumentParser()
    parser.add_argument('-m', '--metrics', nargs='+',
                        default=['emittance_rms', 'emittance_90'],
                        choices=['emittance_rms', 'emittance_90',
                                 'rms_dpp', 'phase_space', 'time_proj',
                                 'discrepancy'],
                        help='Which metrics to include in the optimization'
                             'of grid parameters.')
    parser.add_argument('-c', '--constraints', default=0., type=float)
    parser.add_argument('dataframe',
                        help='The dataframe to analyze')

    args = parser.parse_args()

    df = pd.read_pickle(args.dataframe)

    metrics = []
    for metric in args.metrics:
        if f'diff_{metric}' not in df:
            raise ValueError(f'{metric} is not a metric in the dataframe.')
        metrics.append(f'diff_{metric}')

    # minimisation
    df['square_sum'] = 0.
    for metric in metrics:
        if 'emittance' in metric or 'rms' in metric:
            df['square_sum'] += np.power(df[metric] - 1, 2)
        else:
            df['square_sum'] += np.power(df[metric], 2)

    if args.constraints > 0:
        df = df[df['time'] < args.constraints].dropna()

    df = df.sort_values('square_sum')

    # drop out excessive columnts
    df = df[df.columns.drop(list(df.filter(regex='diff')))]
    df = df.drop('label', axis=1)

    print(df[:10])


if __name__ == '__main__':
    main()
