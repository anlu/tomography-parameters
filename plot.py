#!/usr/bin/env python3
"""
Simple script for loading a pickled pandas dataframe containing results from
a grid search and plotting it.
"""

from argparse import ArgumentParser

import matplotlib.pyplot as plt
import pandas as pd

import utils

parser = ArgumentParser()
parser.add_argument('dataframe', type=str, help='Path to a pandas dataframe'
                                                'to plot')
parser.add_argument('-t', '--max-time', type=float, required=False, dest='t',
                    help='Maximum time (x axis)')
parser.add_argument('-d', '--max-discrepancy', type=float, required=False,
                    dest='d',
                    help='Maximum discrepancy (y axis)')
parser.add_argument('-g', '--group', nargs='+', required=False,
                    help='Which varied parameters to group together'
                         '(same colors)')
parser.add_argument('-s', '--suptitle', type=str, required=False,
                    help='Title of the plot')
parser.add_argument('-m', '--metric', type=str,
                    choices=['discrepancy', 'phase_space', 'time_proj',
                             'emittance_rms', 'emittance_90', 'rms_dpp'],
                    default='phase_space',
                    help='Which discrepancy metric to plot')
parser.add_argument('--scatter', nargs=2, required=False,
                    default=None,
                    help='Scatter plot two metrics')

args = parser.parse_args()

df = pd.read_pickle(args.dataframe)

fig = utils.show_runtime_vs_discrepancy(df, interactive=True,
                                        max_time=args.t,
                                        max_discrepancy=args.d,
                                        color_groups=args.group,
                                        title=args.suptitle,
                                        metric=args.metric,
                                        scatter=args.scatter)
plt.show()
