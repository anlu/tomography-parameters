# Automated tomography parameter search

A simple program to execute a grid search on longitudinal tomography parameters
using either BLonD simulation inputs with real voltage functions or .dat files

Run the program as
```shell script
python main.py [--parameters parameters.yml]
```

## HOWTO

The program runs in 2 modes: simulation+reconstruction or reconstruction only.

### Simulation & reconstruction

For simulation+reconstruction, the program is run as following:

```shell script
python main.py simulation --accelerator [psb,ps,sps,lhc] --cycle [accelerator_cycle] --timeframe [--parameters parameters.yml]
```
only psb is supported currently, however.

The accelerator should be registered in [rings.yml](rf_programs/rings.yml),
and requires a BLonDFactory class inherited from [abstract_blondfactory.py](rf_programs/abstract_blondfactory.py).

Furthermore, each beam cycle needs to be configured in `rf_programs/[accelerator]/beam_parameters.yml`,
with at least the emittance and mu values. See the [PSB beam_parameters](rf_programs/psb/beam_parameters.yml) for an example.
One instance from the `beam_parameters.yml` will be used for the grid search, and
the selected instance is inferred from the arguments passed to the program in
the fashion `[args.cycle]_[args.timeframe]`. For instance `BCMS_init`.

The BLonDFactory class that the user provides can be written to load the
voltage, phase and momentum functions however the developer wishes.

#### What it does

The program will simulate the particle beam using BLonD, output the waterfall.
For each set of tomography parameters, the particles will be tracked, and the
phase space reconstructed.
The program will then proceed with postprocessing and calculate the RMS emittance,
90% emittance and RMS dp/p, as well as the L2 distance between the calculated 
and simulated phase space 
and time projections respectively. 

The results of the grid search will be saved to
`logs/[timestamp]_[accelerator]_[cycle]_[timeframe]/results.pkl`
to be used for later plotting or analysis.

### Simulation only

For running the reconstruction only, the program requires an `input.dat` file
with the machine parameters and tracked profiles. 
The program is then run as following:

```shell script
python main.py dat [dat-file] [--parameters parameters.yml]
```

In this mode the loaded waterfall is rebinned, cut according to the grid
parameters, and then the phase space is reconstructed. The only saved metric
is the last discrepancy returned by the reconstruction algorithm.

The results of the grid search will be saved to
`logs/[timestamp]_[dat-file]/results.pkl`
to be used for later plotting or analysis.

## Grid parameters

The grid parameters have to be specified in the `parameters.yml` under the
parameters section. The parameters are separated into tracking (BLonD) and
tomography.

The available parameters are:

#### Tracking:
The tracking parameters are only required when running a grid search with a
BLonD simulation.
* **turns_per_prof**  
Number of turns between each profile recorded during simulation.  
If unset, or set to `-1`, the value defaults to `5*tracking_duration/sync_tune`.
* **n_slices**  
Number of slices per profile during BLonD tracking  
If unset, the behavior is determined by the BLonD factory for each cycle.
* **initial_time**  
When in the cycle to start simulation.  
If unset, this defaults to the time set in `rf_programs/[accelerator]/beam_parameters.yml`
* **final_time**  
When in the cycle to end simulation.  
If unset, this defaults to the time set in `rf_programs/[accelerator]/beam_parameters/yml`
* **tracking_duration**
How long to track particles. This is relative to `initial_time`.  
If unset, this defaults to the time set in `rf_programs/[accelerator]/beam_parameters.yml`
* **n_macro**  
Number of macroparticles used during simulation.  
If unset, this defaults to the value set in `rf_programs/[accelerator]/common_parameters.yml`

Tomography:
* **sync_part_x**  
The bin number of the synchronous particle.  
If unset, or set to `-1`, the value is automatically fitted during runtime.
* **rebin**  
The rebin factor to use on the waterfall during pre-processing.  
Must be set.
* **snpt**  
Square root of particles pr. cell of phase space.  
Must be set
* **cut_left**  
How many bins to the left of the waterfall to cut out.  
If unset, or set to `-1`, will be determined by a naive method during runtime.
* **cut_right**  
How many bins to the right of the waterfall to cut out.  
If unset, or set to `-1`, will be determined by a naive method during runtime.
* **n_iter**
Number of iterations to run the reconstruction loop.  
Must be set.

## Plotting

There exists a [plot.py](plot.py) that plots difference metric vs runtime.
It is run in the following fashion:

```shell script
python plot.py [path-to-results.pkl] -m {phase_space, time_proj, emittance_rms, emittance_90, rms_dpp}
```

The plot script only plots one metric at a time.

* The `-t` argument is used to specify a max runtime duration. The plot axis will
be reduced to the specified value.
* The `-d` argument is used to specify a max difference value. The plot y axis
will be reduced to the specified value.
* The `--group` argument can be used to group points by varied parameter. 
For example `--group snpt rebin` would color groups of points which share the 
same `snpt` and `rebin` parameter values with the same color for easier identification.
* The `-s` argument adds a suptitle to the plot for easier identification.
